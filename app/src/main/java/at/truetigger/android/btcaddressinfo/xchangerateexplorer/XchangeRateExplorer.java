package at.truetigger.android.btcaddressinfo.xchangerateexplorer;

public interface XchangeRateExplorer {

    String CURRENCY_DOLLAR = "USD";
    String CURRENCY_EURO = "EUR";

    void requestBtcRates(XchangeRateExplorerCallback callback, int requestCode);

}
