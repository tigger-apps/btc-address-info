package at.truetigger.android.btcaddressinfo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.AddressInfo;
import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.blockexplorer.BlockExplorer;
import at.truetigger.android.btcaddressinfo.blockexplorer.BlockExplorerCallback;
import at.truetigger.android.btcaddressinfo.blockexplorer.BtcAddressInfo;
import at.truetigger.android.btcaddressinfo.db.AddressDbService;
import at.truetigger.android.btcaddressinfo.db.XChangeRateDbService;
import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;
import at.truetigger.android.btcaddressinfo.util.BtcFormatter;
import at.truetigger.android.btcaddressinfo.util.ConnectivityChecker;
import at.truetigger.android.btcaddressinfo.util.FiatFormatter;
import at.truetigger.android.btcaddressinfo.xchangerateexplorer.XchangeRateExplorer;
import at.truetigger.android.btcaddressinfo.xchangerateexplorer.XchangeRateExplorerCallback;

public class AddressInfoActivity extends AppCompatActivity implements
        BlockExplorerCallback,
        XchangeRateExplorerCallback,
        AddressDbService.AddressDbServiceCallback,
        XChangeRateDbService.XChangeRateServiceCallback {

    public static final String EXTRA_ADDRESS = "raw_address";

    private static final String TAG = "AddressInfoActivity";

    private static final int RC_ADDRESS_DATA          = 201;
    private static final int RC_XCHANGE_RATE_DATA     = 202;
    private static final int RC_DB_READ_ADDRESS       = 203;
    private static final int RC_DB_READ_XCHANGE_RATE  = 204;
    private static final int RC_DB_STORE_ADDRESS      = 205;
    private static final int RC_DB_STORE_XCHANGE_RATE = 206;
    private static final int RC_DB_REMOVE_ADDRESS     = 207;

    private ProgressBar progressBar;
    private TextView customName;
    private TextView addressOutput;
    private TextView numTxOutput;
    private TextView fundsInCrypto;
    private TextView fundsInFiat;
    private Button showQrCodeButton;
    private Button refreshInfoButton;
    private Button removeAddressButton;
    private TextView statusBar;

    private ConnectivityChecker connectivityChecker;
    private BlockExplorer blockExplorer;
    private XchangeRateExplorer xchangeRateExplorer;
    private AddressDbService addressDbService;
    private XChangeRateDbService xChangeRateDbService;

    private BigDecimal lastReceivedBtcAmount;
    private String lastReceivedBtcAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addressinfo);
        initServices();
        bindComponents();
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String address = getAddressFromExtras();
        if (address == null) {
            showStatus(getResources().getString(R.string.status_no_address));
        } else {
            toggleProgressBar(true);
            addressDbService.getAddressDetails(address, this, RC_DB_READ_ADDRESS);
        }
    }

    @Override
    protected void onStop() {
        clearStatus();
        super.onStop();
    }

    @Override
    public void onServiceSuccess(int requestCode) {
        if (requestCode == RC_DB_REMOVE_ADDRESS) {
            finish(); // back to previous activity
        } else {
            Log.e(TAG, "Unexpected service success for RC " + requestCode);
            showStatus(getResources().getString(R.string.status_general_error));
        }
    }

    @Override
    public void onServiceSuccess(int requestCode, BtcAddressInfo btcAddressInfo) {
        showStatus(getResources().getString(R.string.status_web_address_info_received));
        addressOutput.setText(btcAddressInfo.getAddress());
        numTxOutput.setText(formatNumTransactions(btcAddressInfo.getNumTransactions(), btcAddressInfo.getNumFundedTXO(), btcAddressInfo.getNumSpentTXO()));
        fundsInCrypto.setText(BtcFormatter.formatCrypto(btcAddressInfo.getSumAmount()));
        fundsInFiat.setText(R.string.fiat_refresh_info);
        xChangeRateDbService.findLatestXChangeRate(this, RC_DB_READ_XCHANGE_RATE);
        toggleProgressBar(false);
        storeBtcAddressInDb(btcAddressInfo);

        lastReceivedBtcAmount = btcAddressInfo.getSumAmount();
        lastReceivedBtcAddress = btcAddressInfo.getAddress();
    }

    @Override
    public void onServiceSuccess(int requestCode, Address address) {
        if (requestCode == RC_DB_STORE_ADDRESS) {
            Log.d(TAG, "Address record stored: " + address.btcAddress);
        } else {
            Log.e(TAG, "Unexpected address received: requestCode=" + requestCode + ", for address: " + address.btcAddress);
        }
    }

    @Override
    public void onServiceSuccess(int requestCode, Address address, AddressCheck addressCheck) {
        if (((MyApplication) getApplication()).addressInfoCacheTimeExceeded(addressCheck.checkedAt)
                && connectivityChecker.hasNetworkConnectivity()) {
            blockExplorer.requestBtcAddressInfo(addressCheck.btcAddress, this, RC_ADDRESS_DATA);
            return;
        }
        showStatus(getResources().getString(R.string.status_db_address_info_received));
        customName.setText(address.customName);
        addressOutput.setText(address.btcAddress);
        numTxOutput.setText(formatNumTransactions(addressCheck.numTransactions, addressCheck.numFundedTxo, addressCheck.numSpentTxo));
        fundsInCrypto.setText(BtcFormatter.formatCrypto(addressCheck.btcAmount));
        fundsInFiat.setText(R.string.fiat_refresh_info);
        xChangeRateDbService.findLatestXChangeRate(this, RC_DB_READ_XCHANGE_RATE);
        toggleProgressBar(false);

        lastReceivedBtcAmount = addressCheck.btcAmount;
        lastReceivedBtcAddress = addressCheck.btcAddress;

    }

    @Override
    public void onServiceSuccess(int requestCode, List<AddressInfo> addressInfoList) {
        Log.d(TAG, "Not available in this activity");
    }

    @Override
    public void onServiceSuccess(int requestCode, Map<String, BigDecimal> btcRates) {
        BigDecimal btcEuroRate = ((MyApplication) getApplication()).findEuroRateFromValues(btcRates);
        BigDecimal btcDollarRate = ((MyApplication) getApplication()).findDollarRateFromValues(btcRates);
        fundsInFiat.setText(formatFiat(btcEuroRate, btcDollarRate));
        showStatus(getResources().getString(R.string.status_web_btc_rates_received));
        storeRatesInDb(btcRates);
    }

    @Override
    public void onServiceSuccess(int requestCode, Map<String, Object> metaData, XChangeRate xChangeRate) {
        onServiceSuccess(requestCode, xChangeRate);
    }

    @Override
    public void onServiceSuccess(int requestCode, XChangeRate xChangeRate) {
        switch (requestCode) {
            case RC_DB_READ_XCHANGE_RATE:
                if (((MyApplication) getApplication()).xchangeRateCacheTimeExceeded(xChangeRate.checkedAt)
                        && connectivityChecker.hasNetworkConnectivity()) {
                    xchangeRateExplorer.requestBtcRates(this, RC_XCHANGE_RATE_DATA);
                } else {
                    fundsInFiat.setText(formatFiat(xChangeRate.btcEuroRate, xChangeRate.btcDollarRate));
                    showStatus(getResources().getString(R.string.status_db_btc_rates_received));
                }
                break;
            case RC_DB_STORE_XCHANGE_RATE:
                Log.d(TAG, "Stored: " + xChangeRate);
                break;
            default:
                Log.e(TAG, "Unexpected success: rcCode=" + requestCode);
        }
    }

    @Override
    public void onServiceError(int requestCode, Map<String, Object> metaData, int errorCode, String errorMessage) {
        onServiceError(requestCode, errorCode, errorMessage);
    }

    @Override
    public void onServiceError(int requestCode, int errorCode, String errorMessage) {

        if (requestCode == RC_DB_READ_ADDRESS) {
            Log.d(TAG, "Address not known - first scan. Fetch address info from web");
            fetchAddressInfo(); // first scan of address
            return;
        }

        if (requestCode == RC_DB_READ_XCHANGE_RATE) {
            Log.d(TAG, "No XChange rates available - fetch xchange reate from web");
            xchangeRateExplorer.requestBtcRates(this, RC_XCHANGE_RATE_DATA);
            return;
        }

        final int msgId = requestCode == RC_ADDRESS_DATA ? R.string.status_web_address_info_received : R.string.status_err_cant_fetch_rates;
        showStatus(getResources().getString(msgId));
        Log.e(TAG, "Received error: rcCode=" + requestCode + ", errCode=" + errorCode + ", errMsg=" + errorMessage);
    }

    private String getAddressFromExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(EXTRA_ADDRESS)) {
            return extras.getString(EXTRA_ADDRESS);
        }
        return null;
    }

    private void initServices() {
        connectivityChecker = new ConnectivityChecker(this);
        addressDbService = ((MyApplication) getApplication()).getAddressDbService();
        xChangeRateDbService = ((MyApplication) getApplication()).getXChangeRateDbService();
        blockExplorer = ((MyApplication) getApplication()).getBlockExplorer();
        xchangeRateExplorer = ((MyApplication) getApplication()).getXchangeRateExplorer();
    }

    private void bindComponents() {
        progressBar = findViewById(R.id.progressBarInfo);
        customName = findViewById(R.id.out_customName);
        addressOutput = findViewById(R.id.out_address);
        numTxOutput = findViewById(R.id.out_numTx);
        fundsInCrypto = findViewById(R.id.out_fundsInCrypto);
        fundsInFiat = findViewById(R.id.out_fundsInFiat);
        showQrCodeButton = findViewById(R.id.btn_show_qrcode);
        refreshInfoButton = findViewById(R.id.btn_refresh_info);
        removeAddressButton = findViewById(R.id.btn_remove_entry);
        statusBar = findViewById(R.id.statusBarInfo);
    }

    private void initComponents() {
        showQrCodeButton.setOnClickListener(v -> {
            Intent showQrCodeIntent = new Intent(this, ShowQRCodeActivity.class);
            showQrCodeIntent.putExtra(EXTRA_ADDRESS, lastReceivedBtcAddress);
            startActivity(showQrCodeIntent);
        });
        refreshInfoButton.setOnClickListener(v -> {
            clearStatus();
            if (connectivityChecker.hasNetworkConnectivity()) {
                fetchAddressInfo();
            } else {
                showStatus(getResources().getString(R.string.status_no_network_available));
            }
        });
        removeAddressButton.setOnClickListener(v -> {
            if (lastReceivedBtcAddress != null) {
                toggleProgressBar(true);
                addressDbService.removeAddressDetails(lastReceivedBtcAddress, this, RC_DB_REMOVE_ADDRESS);
            }
        });
        clearStatus();
    }

    private void toggleProgressBar(boolean showProgressBar) {
        if (showProgressBar) {
            progressBar.setVisibility(View.VISIBLE);
            refreshInfoButton.setEnabled(false);
            removeAddressButton.setEnabled(false);
            showQrCodeButton.setEnabled(false);
        } else {
            progressBar.setVisibility(View.GONE);
            refreshInfoButton.setEnabled(true);
            removeAddressButton.setEnabled(true);
            showQrCodeButton.setEnabled(true);
        }
    }

    private void fetchAddressInfo() {
        String address = getAddressFromExtras();
        if (address == null) {
            showStatus(getResources().getString(R.string.status_no_address));
        } else if (!connectivityChecker.hasNetworkConnectivity()) {
            showStatus(getResources().getString(R.string.status_no_network_available));;
        } else {
            toggleProgressBar(true);
            blockExplorer.requestBtcAddressInfo(address, this, RC_ADDRESS_DATA);
        }
    }

    private String formatNumTransactions(int numTransactions, int numFundedTxo, int numSpentTxo) {
        return String.format("%d %s (%s: %d)",
                numTransactions, getResources().getQuantityString(R.plurals.num_transactions, numTransactions),
                getResources().getString(R.string.unspent_txo), numFundedTxo - numSpentTxo);
    }

    private String formatFiat(BigDecimal btcEuroRate, BigDecimal btcDollarRate) {
        return formatFiat("$", btcDollarRate) + " / " + formatFiat("€", btcEuroRate);
    }

    private String formatFiat(String currency, BigDecimal rate) {
        return FiatFormatter.formatFiat(currency, lastReceivedBtcAmount.multiply(rate));
    }

    private void storeBtcAddressInDb(BtcAddressInfo btcAddressInfo) {
        addressDbService.storeAddress(btcAddressInfo, this, RC_DB_STORE_ADDRESS);
    }

    private void storeRatesInDb(Map<String, BigDecimal> btcRates) {
        BigDecimal btcDollarRate = ((MyApplication) getApplication()).findDollarRateFromValues(btcRates);
        BigDecimal btcEuroRate = ((MyApplication) getApplication()).findEuroRateFromValues(btcRates);
        xChangeRateDbService.storeXChangeRate(XChangeRate.forEuroAndDollar(btcEuroRate, btcDollarRate), this, RC_DB_STORE_XCHANGE_RATE);
    }

    private void showStatus(String msg) {
        statusBar.setText(msg.trim());
    }

    private void clearStatus() {
        statusBar.setText("");
    }

}