package at.truetigger.android.btcaddressinfo.xchangerateexplorer;

import java.math.BigDecimal;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.ServiceCallback;

public interface XchangeRateExplorerCallback extends ServiceCallback {

    void onServiceSuccess(int requestCode, Map<String, BigDecimal> btcRates);

}
