package at.truetigger.android.btcaddressinfo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

public class AddressInfo {

    private String customName;
    private String btcAddress;
    private BigDecimal lastAmount;
    private Date lastChecked;

    public AddressInfo(String customName, String btcAddress, BigDecimal lastAmount, Date lastChecked) {
        this.customName = customName;
        this.btcAddress = btcAddress;
        this.lastAmount = lastAmount;
        this.lastChecked = lastChecked;
    }

    public Optional<String> getCustomName() {
        return Optional.ofNullable(customName);
    }

    public String getBtcAddress() {
        return btcAddress;
    }

    public BigDecimal getLastAmount() {
        return lastAmount;
    }

    public Date getLastChecked() {
        return lastChecked;
    }
}
