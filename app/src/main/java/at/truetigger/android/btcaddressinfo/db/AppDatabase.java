package at.truetigger.android.btcaddressinfo.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import at.truetigger.android.btcaddressinfo.db.dao.AddressCheckDao;
import at.truetigger.android.btcaddressinfo.db.dao.AddressDao;
import at.truetigger.android.btcaddressinfo.db.dao.XChangeRateDao;
import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

@Database(entities = {Address.class, AddressCheck.class, XChangeRate.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public static final String NAME = "MyAppDB";

    public abstract AddressDao getAddressDao();

    public abstract AddressCheckDao getAddressCheckDao();

    public abstract XChangeRateDao getXChangeRateDao();
}
