package at.truetigger.android.btcaddressinfo.io;

import androidx.annotation.VisibleForTesting;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;
import at.truetigger.android.btcaddressinfo.db.dto.AllData;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

public class CsvDataExporter implements DataExporter {

    private static final String CONTENT_TYPE = "text/csv";
    private static final String CSV_FIELD_SEPARATOR = ",";
    private static final String CSV_RECORD_SEPARATOR = "\r\n";
    private static final String QUOTE_CHARACTER = "\"";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private static final String FILE_EXTENSION = "csv";
    private static final String FILE_NAME_PART_ADDRESSES = "addresses";
    private static final String FILE_NAME_PART_ADDRESS_CHECKS = "address_checks";
    private static final String FILE_NAME_PART_XCHANGE_RATES = "xchange_rates";

    @Override
    public String getContentType() {
        return CONTENT_TYPE;
    }

    @Override
    public Map<String, byte[]> buildExportData(Date exportDateTime, AllData allData) throws IOException {
        final String dateFileNamePart = buildFileNamePart(exportDateTime);
        final Map<String, byte[]> fileMap = new HashMap<>();
        fileMap.put(buildFileName(FILE_NAME_PART_ADDRESSES, dateFileNamePart), buildAddressData(allData.getAddressList()));
        fileMap.put(buildFileName(FILE_NAME_PART_ADDRESS_CHECKS, dateFileNamePart), buildAddressCheckData(allData.getAddressCheckList()));
        fileMap.put(buildFileName(FILE_NAME_PART_XCHANGE_RATES, dateFileNamePart), buildXChangeRateData(allData.getXChangeRateList()));
        return fileMap;
    }

    @VisibleForTesting
    public String formatString(String stringValue) {
        if (stringValue == null) {
            return "";
        }
        final String normalized = replaceNewLines(stringValue);
        return QUOTE_CHARACTER
                + normalized.replace(QUOTE_CHARACTER, QUOTE_CHARACTER + QUOTE_CHARACTER)
                + QUOTE_CHARACTER;
    }

    @VisibleForTesting
    public String formatDate(Date dateValue) {
        if (dateValue == null) {
            return "";
        }
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(dateValue.toInstant(), ZoneId.of("UTC"));
        return zonedDateTime.format(DATE_FORMATTER);
    }

    @VisibleForTesting
    public String formatLong(Long longValue) {
        if (longValue == null) {
            return "";
        }
        return longValue.toString();
    }

    @VisibleForTesting
    String formatBigDecimal(BigDecimal bigDecimalValue) {
        if (bigDecimalValue == null) {
            return "";
        }
        return QUOTE_CHARACTER + bigDecimalValue.toPlainString() + QUOTE_CHARACTER;
    }

    private String replaceNewLines(String input) {
        String output = input.replace("\r\n", " ");
        output = output.replace("\r", " ");
        return output.replace("\n", " ");
    }

    private String buildFileNamePart(Date exportDateTime) {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm");
        final ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(exportDateTime.toInstant(), ZoneId.of("UTC"));
        return zonedDateTime.format(dateTimeFormatter);
    }

    private byte[] buildAddressData(List<Address> addressList) throws IOException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        writer.write("btc_address" + CSV_FIELD_SEPARATOR
                + "custom_name" + CSV_FIELD_SEPARATOR
                + "address_type" + CSV_FIELD_SEPARATOR
                + "address_check_id" + CSV_FIELD_SEPARATOR
                + "created_at" + CSV_RECORD_SEPARATOR);
        for (Address address : addressList) {
            writer.write(formatString(address.btcAddress) + CSV_FIELD_SEPARATOR
                    + formatString(address.customName) + CSV_FIELD_SEPARATOR
                    + formatString(address.addressType) + CSV_FIELD_SEPARATOR
                    + formatLong(address.addressCheckId) + CSV_FIELD_SEPARATOR
                    + formatDate(address.createdAt) + CSV_RECORD_SEPARATOR);
        }
        writer.close();
        return outputStream.toByteArray();
    }

    private byte[] buildAddressCheckData(List<AddressCheck> addressCheckList) throws IOException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        writer.write("id" + CSV_FIELD_SEPARATOR
                + "btc_address" + CSV_FIELD_SEPARATOR
                + "btc_amount" + CSV_FIELD_SEPARATOR
                + "num_transactions" + CSV_FIELD_SEPARATOR
                + "num_funded_txo" + CSV_FIELD_SEPARATOR
                + "num_spent_txo" + CSV_FIELD_SEPARATOR
                + "checked_at" + CSV_RECORD_SEPARATOR);
        for (AddressCheck addressCheck : addressCheckList) {
            writer.write(formatLong(addressCheck.id) + CSV_FIELD_SEPARATOR
                    + formatString(addressCheck.btcAddress) + CSV_FIELD_SEPARATOR
                    + formatBigDecimal(addressCheck.btcAmount) + CSV_FIELD_SEPARATOR
                    + formatLong(addressCheck.numTransactions.longValue()) + CSV_FIELD_SEPARATOR
                    + formatLong(addressCheck.numFundedTxo.longValue()) + CSV_FIELD_SEPARATOR
                    + formatLong(addressCheck.numSpentTxo.longValue()) + CSV_FIELD_SEPARATOR
                    + formatDate(addressCheck.checkedAt) + CSV_RECORD_SEPARATOR);
        }
        writer.close();
        return outputStream.toByteArray();
    }

    private byte[] buildXChangeRateData(List<XChangeRate> xChangeRateList) throws IOException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        writer.write("id" + CSV_FIELD_SEPARATOR
                + "btc_euro_rate" + CSV_FIELD_SEPARATOR
                + "btc_dollar_rate" + CSV_FIELD_SEPARATOR
                + "checked_at" + CSV_RECORD_SEPARATOR);
        for (XChangeRate xChangeRate : xChangeRateList) {
            writer.write(formatLong(xChangeRate.id) + CSV_FIELD_SEPARATOR
                    + formatBigDecimal(xChangeRate.btcEuroRate) + CSV_FIELD_SEPARATOR
                    + formatBigDecimal(xChangeRate.btcDollarRate) + CSV_FIELD_SEPARATOR
                    + formatDate(xChangeRate.checkedAt) + CSV_RECORD_SEPARATOR);
        }
        writer.close();
        return outputStream.toByteArray();
    }

    private String buildFileName(String tableNamePart, String dateFileNamePart) {
        return String.format("%s_%s.%s", dateFileNamePart, tableNamePart, FILE_EXTENSION);
    }
}
