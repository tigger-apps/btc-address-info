package at.truetigger.android.btcaddressinfo.activity;

import static at.truetigger.android.btcaddressinfo.util.XChangeStatisticsFragmentAdapter.TAB_NAMES;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.util.XChangeStatisticsFragmentAdapter;

public class XChangeStatisticsActivity extends AppCompatActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xchange_statistics);

        TabLayout tabLayout = findViewById(R.id.tablayout);
        ViewPager2 viewPager = findViewById(R.id.viewpager);
        viewPager.setUserInputEnabled(false); // disable swiping
        XChangeStatisticsFragmentAdapter adapter = new XChangeStatisticsFragmentAdapter(getSupportFragmentManager(), getLifecycle());
        viewPager.setAdapter(adapter);

        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> tab.setText(getResources().getText(TAB_NAMES[position])));
        tabLayoutMediator.attach();
    }
}
