package at.truetigger.android.btcaddressinfo.util;

import androidx.annotation.NonNull;

import java.time.Duration;
import java.util.Date;

public class DurationToDateConverter {

    /**
     * Returns a Date object with timestamp NOW - given duration
     *
     * @param baseDate
     * @param duration
     * @return
     */
    public static Date dateMinusDuration(@NonNull Date  baseDate, @NonNull Duration duration) {
        final long nowMillis = baseDate.getTime();
        final long durationMillis = duration.toMillis();
        return new Date(nowMillis - durationMillis);
    }
}
