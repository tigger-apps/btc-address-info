package at.truetigger.android.btcaddressinfo.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

import java.math.BigDecimal;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.db.XChangeRateDbService;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;
import at.truetigger.android.btcaddressinfo.util.ConnectivityChecker;
import at.truetigger.android.btcaddressinfo.xchangerateexplorer.XchangeRateExplorer;
import at.truetigger.android.btcaddressinfo.xchangerateexplorer.XchangeRateExplorerCallback;

public class RefreshRatesJobService extends JobService implements
        XChangeRateDbService.XChangeRateServiceCallback,
        XchangeRateExplorerCallback {

    private static final int RC_XCHANGE_RATE_DATA     = 804;
    private static final int RC_DB_STORE_XCHANGE_RATE = 805;

    private static final String TAG = "RefreshRatesJobService";

    private JobParameters jobParameters;
    private ConnectivityChecker connectivityChecker;
    private XchangeRateExplorer xchangeRateExplorer;
    private XChangeRateDbService xChangeRateDbService;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "Job started");
        this.jobParameters = params;
        initServices();
        refreshXChangeRate();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        jobParameters = null;
        return false;
    }

    @Override
    public void onServiceSuccess(int requestCode, Map<String, BigDecimal> btcRates) {
        BigDecimal btcEuroRate = ((MyApplication) getApplication()).findEuroRateFromValues(btcRates);
        BigDecimal btcDollarRate = ((MyApplication) getApplication()).findDollarRateFromValues(btcRates);
        XChangeRate xChangeRate = XChangeRate.forEuroAndDollar(btcEuroRate, btcDollarRate);
        xChangeRateDbService.storeXChangeRate(xChangeRate, this, RC_DB_STORE_XCHANGE_RATE);
    }

    @Override
    public void onServiceSuccess(int requestCode, Map<String, Object> metaData, XChangeRate xChangeRate) {
        onServiceSuccess(requestCode, xChangeRate);
    }

    @Override
    public void onServiceSuccess(int requestCode, XChangeRate xChangeRate) {
        Log.d(TAG, "New xChangeRate stored: " + xChangeRate.btcDollarRate);
        markJobDone();
    }

    @Override
    public void onServiceSuccess(int requestCode) {
        Log.e(TAG, "Unexpected service success for requestCode[" + requestCode + "]");
        markJobDone();
    }

    @Override
    public void onServiceError(int requestCode, Map<String, Object> metaData, int errorCode, String errorMessage) {
        onServiceError(requestCode, errorCode, errorMessage);
    }

    @Override
    public void onServiceError(int requestCode, int errorCode, String errorMessage) {
        Log.e(TAG, "ServiceError: rcCode=" + requestCode + ", errorCode=" + errorCode + ", errorMsg=" + errorMessage);
        markJobDone();
    }

    public void refreshXChangeRate() {
        Log.d(TAG, "Try to refresh xChange rate");
        if (connectivityChecker.hasNetworkConnectivity()) {
            xchangeRateExplorer.requestBtcRates(this, RC_XCHANGE_RATE_DATA);
        } else {
            markJobDone();
        }
    }

    private void markJobDone() {
        if (jobParameters != null) {
            jobFinished(jobParameters, false);
            Log.d(TAG, "Job done");
        }
    }

    private void initServices() {
        connectivityChecker = ((MyApplication) getApplication()).getConnectivityChecker();
        xchangeRateExplorer = ((MyApplication) getApplication()).getXchangeRateExplorer();
        xChangeRateDbService = ((MyApplication) getApplication()).getXChangeRateDbService();
    }
}
