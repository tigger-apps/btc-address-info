package at.truetigger.android.btcaddressinfo.db;

import java.util.Map;

import at.truetigger.android.btcaddressinfo.ServiceCallback;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

public class XChangeRateDbService extends AbstractDbService {

    public static final int ERR_CODE_CANNOT_STORE = 3001;
    public static final int ERR_CODE_CANNOT_FIND_LATEST_ENTRY = 3002;

    public XChangeRateDbService(AppDatabase database) {
        super(database);
    }

    public void storeXChangeRate(XChangeRate xChangeRate, XChangeRateServiceCallback callback, int requestCode) {
        Runnable storeXChangeRateThread = () -> {
            try {
                Long xChangeRateId = getXChangeRateDao().insert(xChangeRate);
                if (xChangeRateId == null) {
                    onServiceError(callback, requestCode, ERR_CODE_CANNOT_STORE, "no id generated");
                    return;
                }
                xChangeRate.id = xChangeRateId;
                onServiceSuccess(callback, requestCode, xChangeRate);
            } catch (Exception exception) {
                onServiceError(callback, requestCode, ERR_CODE_CANNOT_STORE, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(storeXChangeRateThread).start();
    }

    public void findLatestXChangeRate(XChangeRateServiceCallback callback, int requestCode) {
        Runnable findXChangeRateThread = () -> {
            try {
                XChangeRate xChangeRate = getXChangeRateDao().findLatestXChangeRate();
                if (xChangeRate == null) {
                    onServiceError(callback, requestCode, ERR_CODE_CANNOT_FIND_LATEST_ENTRY, "no entry found");
                } else {
                    onServiceSuccess(callback, requestCode, xChangeRate);
                }
            } catch (Exception exception) {
                onServiceError(callback, requestCode, ERR_CODE_CANNOT_FIND_LATEST_ENTRY, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(findXChangeRateThread).start();
    }

    public void findLatestXchangeRate(XChangeRateServiceCallback callback, int requestCode, Map<String, Object> metaData) {
        Runnable findXChangeRateThread = () -> {
            try {
                XChangeRate xChangeRate = getXChangeRateDao().findLatestXChangeRate();
                if (xChangeRate == null) {
                    onServiceError(callback, requestCode, metaData, ERR_CODE_CANNOT_FIND_LATEST_ENTRY, "no entry found");
                } else {
                    onServiceSuccess(callback, requestCode, metaData, xChangeRate);
                }
            } catch (Exception exception) {
                onServiceError(callback, requestCode, metaData, ERR_CODE_CANNOT_FIND_LATEST_ENTRY, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(findXChangeRateThread).start();
    }

    private void onServiceSuccess(XChangeRateServiceCallback callback, int requestCode, XChangeRate xChangeRate) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, xChangeRate);
        });
    }

    private void onServiceSuccess(XChangeRateServiceCallback callback, int requestCode, Map<String, Object> metaData, XChangeRate xChangeRate) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, metaData, xChangeRate);
        });
    }

    private void onServiceError(XChangeRateServiceCallback callback, int requestCode, Map<String, Object> metaData, int errorCode, String errorMsg) {
        runOnMainThread(() -> {
            callback.onServiceError(requestCode, metaData, errorCode, errorMsg);
        });
    }

    public interface XChangeRateServiceCallback extends ServiceCallback {
         void onServiceSuccess(int requestCode, XChangeRate xChangeRate);
         void onServiceSuccess(int requestCode, Map<String, Object> metaData, XChangeRate xChangeRate);
         void onServiceError(int requestCode, Map<String, Object> metaData, int errorCode, String errorMsg);
    }
}
