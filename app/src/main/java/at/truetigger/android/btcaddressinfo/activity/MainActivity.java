package at.truetigger.android.btcaddressinfo.activity;

import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.AddressInfo;
import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.db.AddressDbService;
import at.truetigger.android.btcaddressinfo.db.dto.AllData;
import at.truetigger.android.btcaddressinfo.db.AllDataDbService;
import at.truetigger.android.btcaddressinfo.db.XChangeRateDbService;
import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;
import at.truetigger.android.btcaddressinfo.io.ImportExportService;
import at.truetigger.android.btcaddressinfo.io.JsonDataExporter;
import at.truetigger.android.btcaddressinfo.util.AddressInfoAdapter;
import at.truetigger.android.btcaddressinfo.util.BackgroundControlService;
import at.truetigger.android.btcaddressinfo.util.BtcAddressExtractor;
import at.truetigger.android.btcaddressinfo.util.ConnectivityChecker;
import at.truetigger.android.btcaddressinfo.util.FiatFormatter;
import at.truetigger.android.btcaddressinfo.widget.XChangeRateWidget;
import at.truetigger.android.btcaddressinfo.xchangerateexplorer.XchangeRateExplorer;
import at.truetigger.android.btcaddressinfo.xchangerateexplorer.XchangeRateExplorerCallback;

public class MainActivity extends AppCompatActivity implements
        AddressDbService.AddressDbServiceCallback,
        XChangeRateDbService.XChangeRateServiceCallback,
        AllDataDbService.AllDataDbServiceCallback,
        XchangeRateExplorerCallback,
        ImportExportService.ImportExportServiceCallback {

    private static final String TAG = "MainActivity";

    public static final String CLOSE_IMMEDIATELY = "CLOSE_APP";
    private static final int CAMERA_ID = -1; // means: no default

    private static final int RC_DB_FETCH_ADDRESSES    = 101;
    private static final int RC_DB_UPDATE_CUSTOMNAME  = 102;
    private static final int RC_DB_READ_XCHANGE_RATE  = 103;
    private static final int RC_XCHANGE_RATE_DATA     = 104;
    private static final int RC_DB_STORE_XCHANGE_RATE = 105;
    private static final int RC_DB_EXPORT_READDATA    = 106;
    private static final int RC_DB_EXPORT_CREATEFILE  = 107;
    private static final int RC_DB_EXPORT_WRITE       = 108;
    private static final int RC_CLEAR_DATABASE        = 109;
    private static final int RC_DB_BACKUP_READDATA    = 110;
    private static final int RC_DB_BACKUP_WRITE       = 111;
    private static final int RC_DB_RESTORE_OPENFILE   = 112;
    private static final int RC_DB_RESTORE_EXTRACTED  = 113;
    private static final int RC_DB_RESTORE_APPLIED    = 114;

    private TextView xchangeRateOutput;
    private Button scanButton;
    private TextView scanErrorView;
    private ListView addressListView;
    private TextView statusBar;

    private ConnectivityChecker connectivityChecker;
    private BtcAddressExtractor btcAddressExtractor;
    private XchangeRateExplorer xchangeRateExplorer;
    private AddressDbService addressDbService;
    private XChangeRateDbService xChangeRateDbService;
    private AllDataDbService allDataDbService;
    private ImportExportService importExportService;
    private AddressInfoAdapter addressInfoAdapter;
    private BackgroundControlService backgroundControlService;

    private String exportFormat;
    private Map<String, byte[]> exportData;
    private String exportContentType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initServices();
        if (getIntent().getBooleanExtra(CLOSE_IMMEDIATELY, false)) {
            finish();
        }
        updateWidget();
        bindComponents();
        initComponents();
        startBackgroundServices();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadAddressListFromDb();
        loadLatestXchangeRateFromDb();
    }

    @Override
    protected void onStop() {
        super.onStop();
        clearStatus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {

            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            case R.id.menu_rate_statistics:
                startActivity(new Intent(this, XChangeStatisticsActivity.class));
                return true;

            case R.id.menu_export_csv:
                clearExportData();
                exportFormat = ImportExportService.EXPORT_FORMAT_CSV;
                allDataDbService.readAllData(this, RC_DB_EXPORT_READDATA);
                showStatus(getResources().getString(R.string.status_export_started));
                return true;

            case R.id.menu_export_json:
                clearExportData();
                exportFormat = ImportExportService.EXPORT_FORMAT_JSON;
                allDataDbService.readAllData(this, RC_DB_EXPORT_READDATA);
                showStatus(getResources().getString(R.string.status_export_started));
                return true;

            case R.id.menu_backup:
                clearExportData();
                allDataDbService.readAllData(this, RC_DB_BACKUP_READDATA);
                showStatus(getResources().getString(R.string.status_backup_started));
                return true;

            case R.id.menu_restore:
                readBackupFile();
                return true;

            case R.id.menu_clear_db:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.dialog_title_cleardb)
                        .setMessage(R.string.dialog_title_confirmquestions)
                        .setPositiveButton(R.string.dialog_ok, (dialog, itemPosition) -> clearDatabase())
                        .setNegativeButton(R.string.dialog_cancel, (dialog, itemPosition) -> Log.d(TAG, "Cancel clearDB"));
                AlertDialog dialog  = builder.create();
                dialog.show();
                return true;

            default:
                showStatus("Unmapped menu item #" + menuItem.getItemId() + " / " + menuItem.getTitle());
                return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RC_DB_RESTORE_OPENFILE) {
            readFileFromSelectedLocation(data);

        } else if (requestCode == RC_DB_EXPORT_CREATEFILE) {
            writeExportFileToSelectedLocation(data);

        } else {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result.getContents() == null) {
                showStatus(getResources().getString(R.string.status_scan_aborted));
            } else {
                showStatus(getResources().getString(R.string.status_scan_result) + " " + result.getContents());
                processAddress(result.getContents(), false);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onServiceSuccess(int requestCode) {
        switch (requestCode) {
            case RC_DB_EXPORT_WRITE:
                showStatus(getResources().getString(R.string.status_export_finished));
            case RC_DB_UPDATE_CUSTOMNAME:
            case RC_CLEAR_DATABASE:
            case RC_DB_RESTORE_APPLIED:
                loadAddressListFromDb();
                break;
            default:
                Log.d(TAG, "Unexpected generic service success: rcCode=" + requestCode);
        }
    }

    @Override
    public void onServiceSuccess(int requestCode, Address address) {
        Log.d(TAG, "Unexpected service success with address: rcCode=" + requestCode);
    }

    @Override
    public void onServiceSuccess(int requestCode, Address address, AddressCheck addressCheck) {
        Log.d(TAG, "Unexpected service success with address + addressCheck: rcCode=" + requestCode);
    }

    @Override
    public void onServiceSuccess(int requestCode, List<AddressInfo> addressInfoList) {
        addressInfoAdapter.clear();
        addressInfoList.forEach(addressInfo -> addressInfoAdapter.add(addressInfo));
        addressInfoAdapter.notifyDataSetChanged();
        showStatus(getResources().getString(R.string.status_addresses_loaded));
    }

    @Override
    public void onServiceSuccess(int requestCode, Map<String, Object> metaData, XChangeRate xChangeRate) {
        onServiceSuccess(requestCode, xChangeRate);
    }

    @Override
    public void onServiceSuccess(int requestCode, XChangeRate xChangeRate) {
        switch (requestCode) {
            case RC_DB_READ_XCHANGE_RATE:
                if (((MyApplication) getApplication()).xchangeRateCacheTimeExceeded(xChangeRate.checkedAt)
                        && connectivityChecker.hasNetworkConnectivity()) {
                    xchangeRateExplorer.requestBtcRates(this, RC_XCHANGE_RATE_DATA);
                } else {
                    showXchangeRate(xChangeRate);
                    showStatus(getResources().getString(R.string.status_xchangerate_loaded));
                }
                break;

            case RC_DB_STORE_XCHANGE_RATE:
                showXchangeRate(xChangeRate);
                showStatus(getResources().getString(R.string.status_xchangerate_loaded));
                break;

            default:
                Log.e(TAG, "Service error: unexpected xChangeRate received, rcCode=" + requestCode);
        }
    }

    @Override
    public void onServiceSuccess(int requestCode, Map<String, BigDecimal> btcRates) {
        BigDecimal btcEuroRate = ((MyApplication) getApplication()).findEuroRateFromValues(btcRates);
        BigDecimal btcDollarRate = ((MyApplication) getApplication()).findDollarRateFromValues(btcRates);
        XChangeRate xChangeRate = XChangeRate.forEuroAndDollar(btcEuroRate, btcDollarRate);
        xChangeRateDbService.storeXChangeRate(xChangeRate, this, RC_DB_STORE_XCHANGE_RATE);
    }

    @Override
    public void onServiceSuccess(int requestCode, AllData allData) {
        switch (requestCode) {
            case RC_DB_EXPORT_READDATA:
                if (exportFormat != null) {
                    importExportService.export(exportFormat, allData, this, RC_DB_EXPORT_WRITE);
                }
                break;
            case RC_DB_BACKUP_READDATA:
                importExportService.backup(this, allData, this, RC_DB_BACKUP_WRITE);
                break;
            case RC_DB_RESTORE_EXTRACTED:
                allDataDbService.restoreAllData(allData, this, RC_DB_RESTORE_APPLIED);
        }
    }

    @Override
    public void onServiceSuccess(int requestCode, String contentType, Map<String, byte[]> fileData) {
        exportContentType = contentType;
        exportData = fileData;
        writeNextExportFile();
    }

    @Override
    public void onServiceError(int requestCode, Map<String, Object> metaData, int errorCode, String errorMessage) {
        onServiceError(requestCode, errorCode, errorMessage);
    }

    @Override
    public void onServiceError(int requestCode, int errorCode, String errorMessage) {
        switch (requestCode) {
            case RC_DB_READ_XCHANGE_RATE:
                Log.d(TAG, "No XChange rates available - fetch xchange rate from web");
                if (connectivityChecker.hasNetworkConnectivity()) {
                    xchangeRateExplorer.requestBtcRates(this, RC_XCHANGE_RATE_DATA);
                } else {
                    xchangeRateOutput.setText(getResources().getString(R.string.info_disconnected));
                    showStatus(getResources().getString(R.string.status_no_network_available));
                }
                break;

            case RC_DB_EXPORT_WRITE:
                Log.e(TAG, "Error while exporting: " + errorMessage);
                showStatus(String.format(getResources().getString(R.string.status_export_failed), errorMessage));
                break;

            default:
                Log.e(TAG, "ServiceError: rcCode=" + requestCode + ", errorCode=" + errorCode + ", errorMsg=" + errorMessage);
        }
    }

    private void updateWidget() {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        int[] widgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(this, XChangeRateWidget.class));
        Intent updateIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds);
        sendBroadcast(updateIntent);
    }

    private void initServices() {
        btcAddressExtractor = new BtcAddressExtractor();
        xchangeRateExplorer = ((MyApplication) getApplication()).getXchangeRateExplorer();
        addressDbService = ((MyApplication) getApplication()).getAddressDbService();
        xChangeRateDbService = ((MyApplication) getApplication()).getXChangeRateDbService();
        allDataDbService = ((MyApplication) getApplication()).getAllDataDbService();
        importExportService = ((MyApplication) getApplication()).getImportExportService();
        connectivityChecker = ((MyApplication) getApplication()).getConnectivityChecker();
        addressInfoAdapter = new AddressInfoAdapter(new ArrayList<AddressInfo>(), this);
        backgroundControlService = ((MyApplication) getApplication()).getBackgroundControlService();
    }

    private void bindComponents() {
        xchangeRateOutput = findViewById(R.id.out_xchangeRate);
        scanButton = findViewById(R.id.btn_scanQrCode);
        scanErrorView = findViewById(R.id.scan_error);
        addressListView = findViewById(R.id.addressList);
        statusBar = findViewById(R.id.statusBarScan);
    }

    private void initComponents() {
        xchangeRateOutput.setOnClickListener(v -> {
            refreshXchangeRate();
        });
        scanButton.setOnClickListener(v -> {
            scanQrCode();
        });
        addressListView.setAdapter(addressInfoAdapter);
    }

    private void startBackgroundServices() {
        if (backgroundControlService.isRateRefreshEnabled()) {
            backgroundControlService.startRateRefresh();
        }
    }

    private void refreshXchangeRate() {
        scanErrorView.setVisibility(View.GONE);
        if (connectivityChecker.hasNetworkConnectivity()) {
            showStatus(getResources().getString(R.string.status_xchangerate_loading));
            xchangeRateOutput.setText(getResources().getString(R.string.fiat_refresh_info));
            xchangeRateExplorer.requestBtcRates(this, RC_XCHANGE_RATE_DATA);
        } else {
            xchangeRateOutput.setText(getResources().getString(R.string.info_disconnected));
            showStatus(getResources().getString(R.string.status_no_network_available));
        }
    }

    private void scanQrCode() {
        scanErrorView.setVisibility(View.GONE);
        IntentIntegrator intentIntegrator = new IntentIntegrator(MainActivity.this);
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        intentIntegrator.setPrompt(getResources().getString(R.string.hint_scan_address));
        intentIntegrator.setCameraId(CAMERA_ID);
        intentIntegrator.setOrientationLocked(false);
        intentIntegrator.setBeepEnabled(false);
        intentIntegrator.initiateScan();
    }

    public void processAddress(String address, boolean storedAddress) {
        String checkedAddress = btcAddressExtractor.extractBtcAddress(address);
        if (checkedAddress == null) {
            scanErrorView.setVisibility(View.VISIBLE);
            scanErrorView.setText(getResources().getString(R.string.info_unregcognized_input));
            showStatus(String.format(getResources().getString(R.string.status_invalid_address), address));
        } else {
            if (connectivityChecker.hasNetworkConnectivity() || storedAddress) {
                Intent intent = new Intent(this, AddressInfoActivity.class);
                intent.putExtra(AddressInfoActivity.EXTRA_ADDRESS, checkedAddress);
                startActivity(intent);
            } else {
                scanErrorView.setVisibility(View.VISIBLE);
                scanErrorView.setText(getResources().getString(R.string.info_disconnected));
                showStatus(getResources().getString(R.string.status_no_network_available));
            }
        }
    }

    public void updateCustomName(String btcAddress, String newCustomName) {
        addressDbService.updateCustomName(btcAddress, newCustomName, this, RC_DB_UPDATE_CUSTOMNAME);
    }

    public void clearDatabase() {
        allDataDbService.clearDb(this, RC_CLEAR_DATABASE);
    }

    private void showXchangeRate(XChangeRate xChangeRate) {
        final String usdRate = FiatFormatter.formatFiat("$", xChangeRate.btcDollarRate);
        final String euroRate = FiatFormatter.formatFiat("€", xChangeRate.btcEuroRate);
        xchangeRateOutput.setText(String.format("%s / %s", usdRate, euroRate));
    }

    private void loadAddressListFromDb() {
        showStatus(getResources().getString(R.string.status_addresses_loading));
        addressDbService.getAllAddresses(this, RC_DB_FETCH_ADDRESSES);
    }

    private void loadLatestXchangeRateFromDb() {
        showStatus(getResources().getString(R.string.status_xchangerate_loading));
        xChangeRateDbService.findLatestXChangeRate(this, RC_DB_READ_XCHANGE_RATE);
    }

    private void writeNextExportFile() {
        if (exportData.isEmpty()) {
            Log.d(TAG, "No (more) export files to write");
            return;
        }
        final Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType(exportContentType);
        intent.putExtra(Intent.EXTRA_TITLE, getNextFileToSave());
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);

        startActivityForResult(intent, RC_DB_EXPORT_CREATEFILE);
    }

    private void readBackupFile() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType(JsonDataExporter.CONTENT_TYPE);
        startActivityForResult(intent, RC_DB_RESTORE_OPENFILE);
    }

    private String getNextFileToSave() {
        if (exportData == null || exportData.isEmpty()) {
            return null;
        }
        return exportData.keySet().iterator().next();
    }

    private void writeExportFileToSelectedLocation(@Nullable Intent data) {
        final String fileName = getNextFileToSave();
        if (fileName == null  || data == null) {
            // aborted or some error
            clearExportData();
            showStatus(getResources().getString(R.string.status_export_aborted));
            return;
        }

        try {
            final OutputStream outputStream = getContentResolver().openOutputStream(data.getData());
            final byte[] fileData = exportData.remove(fileName);
            outputStream.write(fileData);
            outputStream.close();
            if (!exportData.isEmpty()) {
                writeNextExportFile();
            }
        } catch (IOException ioe) {
            Log.e(TAG, "IOError: " + ioe.getClass().getCanonicalName() + " / " + ioe.getMessage());
            showStatus(String.format(getResources().getString(R.string.status_export_failed), ioe.getMessage()));
        }
    }

    private void readFileFromSelectedLocation(@Nullable Intent data) {
        if (data == null) {
            showStatus(getResources().getString(R.string.status_restore_aborted));
            return;
        }
        try {
            final InputStream inputStream = getContentResolver().openInputStream(data.getData());
            final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String currentline;
            while ((currentline = reader.readLine()) != null) {
                stringBuilder.append(currentline + "\n");
            }
            inputStream.close();
            importExportService.restore(stringBuilder.toString(), this, RC_DB_RESTORE_EXTRACTED);
        } catch (IOException ioe) {
            Log.e(TAG, "IOError: " + ioe.getClass().getCanonicalName() + " / " + ioe.getMessage());
            showStatus(String.format(getResources().getString(R.string.status_restore_failed), ioe.getMessage()));
        }
    }

    private void clearExportData() {
        exportData = null;
        exportContentType = null;
        exportFormat = null;
    }

    private void showStatus(String msg) {
        Log.d(TAG, msg);
        statusBar.setText(msg.trim());
    }

    private void clearStatus() {
        statusBar.setText("");
    }
}
