package at.truetigger.android.btcaddressinfo;

import android.app.Application;

import androidx.room.Room;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.blockexplorer.BlockExplorer;
import at.truetigger.android.btcaddressinfo.blockexplorer.BlockstreamBlockExplorer;
import at.truetigger.android.btcaddressinfo.db.AddressDbService;
import at.truetigger.android.btcaddressinfo.db.AllDataDbService;
import at.truetigger.android.btcaddressinfo.db.AppDatabase;
import at.truetigger.android.btcaddressinfo.db.XChangeRateDbService;
import at.truetigger.android.btcaddressinfo.db.XChangeStatisticsDbService;
import at.truetigger.android.btcaddressinfo.io.ImportExportService;
import at.truetigger.android.btcaddressinfo.util.BackgroundControlService;
import at.truetigger.android.btcaddressinfo.util.ConnectivityChecker;
import at.truetigger.android.btcaddressinfo.xchangerateexplorer.CoindeskXchangeRateExplorer;
import at.truetigger.android.btcaddressinfo.xchangerateexplorer.XchangeRateExplorer;

public class MyApplication extends Application {

    private static final long CACHE_MILLISECONDS_ADDRESS_INFO = 24 * 60 * 60 * 1000L; // 24h
    private static final long CACHE_MILLISECONDS_XCHANGE_RATE = 5 * 60 * 1000L; // 5min

    public static final String PREFERENCES_NAME = "BtcPreferences";

    private AddressDbService addressDbService;
    private XChangeRateDbService xChangeRateDbService;
    private XChangeStatisticsDbService xChangeStatisticsDbService;
    private AllDataDbService allDataDbService;
    private BlockExplorer blockExplorer;
    private XchangeRateExplorer xchangeRateExplorer;
    private ImportExportService importExportService;
    private ConnectivityChecker connectivityChecker;
    private BackgroundControlService backgroundControlService;

    @Override
    public void onCreate() {
        super.onCreate();
        final AppDatabase appDatabase = Room.databaseBuilder(this, AppDatabase.class, AppDatabase.NAME)
                .fallbackToDestructiveMigration()
                .build();
        addressDbService = new AddressDbService(appDatabase);
        xChangeRateDbService = new XChangeRateDbService(appDatabase);
        xChangeStatisticsDbService = new XChangeStatisticsDbService(appDatabase);
        allDataDbService = new AllDataDbService(appDatabase);
        blockExplorer = new BlockstreamBlockExplorer();
        xchangeRateExplorer = new CoindeskXchangeRateExplorer();
        importExportService = new ImportExportService();
        connectivityChecker = new ConnectivityChecker(getApplicationContext());
        backgroundControlService = new BackgroundControlService(getApplicationContext());
    }

    public AddressDbService getAddressDbService() {
        return addressDbService;
    }

    public XChangeRateDbService getXChangeRateDbService() {
        return xChangeRateDbService;
    }

    public XChangeStatisticsDbService getXChangeStatisticsDbService() {
        return xChangeStatisticsDbService;
    }

    public AllDataDbService getAllDataDbService() {
        return allDataDbService;
    }

    public BlockExplorer getBlockExplorer() {
        return blockExplorer;
    }

    public XchangeRateExplorer getXchangeRateExplorer() {
        return xchangeRateExplorer;
    }

    public ImportExportService getImportExportService() {
        return importExportService;
    }

    public ConnectivityChecker getConnectivityChecker() {
        return connectivityChecker;
    }

    public BackgroundControlService getBackgroundControlService() {
        return backgroundControlService;
    }

    public boolean addressInfoCacheTimeExceeded(Date date) {
        return cacheTimeExceeded(date, CACHE_MILLISECONDS_ADDRESS_INFO);
    }

    public boolean xchangeRateCacheTimeExceeded(Date date) {
        return cacheTimeExceeded(date, CACHE_MILLISECONDS_XCHANGE_RATE);
    }

    public BigDecimal findDollarRateFromValues(Map<String, BigDecimal> btcRates) {
        return findValueForCurrency(XchangeRateExplorer.CURRENCY_DOLLAR, btcRates);
    }

    public BigDecimal findEuroRateFromValues(Map<String, BigDecimal> btcRates) {
        return findValueForCurrency(XchangeRateExplorer.CURRENCY_EURO, btcRates);
    }

    private boolean cacheTimeExceeded(Date date, long cacheTimeInMilliSeconds) {
        return date.getTime() + cacheTimeInMilliSeconds < new Date().getTime();
    }

    private BigDecimal findValueForCurrency(String currency, Map<String, BigDecimal> btcRates) {
        return btcRates.entrySet().stream()
                .filter(entry -> currency.equals(entry.getKey()))
                .map(entry -> entry.getValue())
                .findAny()
                .orElse(null);
    }
}
