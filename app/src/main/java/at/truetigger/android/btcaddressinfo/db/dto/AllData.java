package at.truetigger.android.btcaddressinfo.db.dto;

import java.util.List;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

public class AllData {

    private final List<Address> addressList;
    private final List<AddressCheck> addressCheckList;
    private final List<XChangeRate> xChangeRateList;
    private final Map<String, Object> metaData;

    public AllData(List<Address> addressList, List<AddressCheck> addressCheckList, List<XChangeRate> xChangeRateList) {
        this(addressList, addressCheckList, xChangeRateList, null);
    }

    public AllData(List<Address> addressList, List<AddressCheck> addressCheckList, List<XChangeRate> xChangeRateList, Map<String, Object> metaData) {
        this.addressList = addressList;
        this.addressCheckList = addressCheckList;
        this.xChangeRateList = xChangeRateList;
        this.metaData = metaData;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public List<AddressCheck> getAddressCheckList() {
        return addressCheckList;
    }

    public List<XChangeRate> getXChangeRateList() {
        return xChangeRateList;
    }

    public Map<String, Object> getMetaData() {
        return metaData;
    }
}
