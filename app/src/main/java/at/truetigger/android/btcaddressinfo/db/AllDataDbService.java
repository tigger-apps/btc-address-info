package at.truetigger.android.btcaddressinfo.db;

import java.util.List;

import at.truetigger.android.btcaddressinfo.ServiceCallback;
import at.truetigger.android.btcaddressinfo.db.dto.AllData;
import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

public class AllDataDbService extends AbstractDbService {

    public AllDataDbService(AppDatabase database) {
        super(database);
    }

    public void clearDb(AllDataDbServiceCallback callback, int requestCode) {
        Runnable deleteDatabaseThread = () -> {
            clearAllTables();
            onServiceSuccess(callback, requestCode);
        };
        new Thread(deleteDatabaseThread).start();
    }

    public void readAllData(AllDataDbServiceCallback callback, int requestCode) {
        Runnable readAllDataThread = () -> {
            List<Address> addressList = getAddressDao().findAll();
            List<AddressCheck> addressCheckList = getAddressCheckDao().findAll();
            List<XChangeRate> xChangeRateList = getXChangeRateDao().findAll();
            AllData allData = new AllData(addressList, addressCheckList, xChangeRateList);
            onServiceSuccess(callback, requestCode, allData);
        };
        new Thread(readAllDataThread).start();
    }

    public void restoreAllData(AllData allData, AllDataDbServiceCallback callback, int requestCode) {
        Runnable restoreDataThread = () -> {
            clearAllTables();
            allData.getAddressCheckList().forEach(addressCheck -> getAddressCheckDao().insert(addressCheck));
            allData.getAddressList().forEach(address -> getAddressDao().insert(address));
            allData.getXChangeRateList().forEach(xChangeRate -> getXChangeRateDao().insert(xChangeRate));
            onServiceSuccess(callback, requestCode);
        };
        new Thread(restoreDataThread).start();
    }

    private void onServiceSuccess(AllDataDbServiceCallback callback, int requestCode, AllData allData) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, allData);
        });
    }

    public interface AllDataDbServiceCallback extends ServiceCallback {
        void onServiceSuccess(int requestCode, AllData allData);
    }
}
