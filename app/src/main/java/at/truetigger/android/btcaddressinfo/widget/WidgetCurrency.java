package at.truetigger.android.btcaddressinfo.widget;

public enum WidgetCurrency {
    USD("$"),
    EUR("€"),
    ;

    private String symbol;

    private WidgetCurrency(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
