package at.truetigger.android.btcaddressinfo.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class FiatFormatter {

    private static final int FIAT_SCALE = 2;
    private static final NumberFormat FIAT_FORMAT = new DecimalFormat("#,###,##0.00");

    public static String formatFiat(String symbol, BigDecimal value) {
        final double fiatAsDouble = value.setScale(FIAT_SCALE, RoundingMode.HALF_UP).doubleValue();
        return String.format("%s %s", symbol, FIAT_FORMAT.format(fiatAsDouble));
    }
}
