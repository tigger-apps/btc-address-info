package at.truetigger.android.btcaddressinfo.io;

import androidx.annotation.VisibleForTesting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.db.dto.AllData;
import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

public class JsonDataExporter implements DataExporter {

    public static final String BACKUP_FILENAME = "btc-address-info-backup.json";

    public static final String CONTENT_TYPE = "application/json";

    private static final String FILE_EXTENSION = "json";
    private static final String JSON_KEY_METADATA = "metadata";
    private static final String JSON_KEY_ADDRESSES = "addresses";
    private static final String JSON_KEY_ADDRESS_CHECKS = "addressChecks";
    private static final String JSON_KEY_XCHANGE_RATES = "xChangeRates";

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    @Override
    public String getContentType() {
        return CONTENT_TYPE;
    }

    @Override
    public Map<String, byte[]> buildExportData(Date exportDateTime, AllData allData) throws IOException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        try {
            final JSONObject jsonRoot = buildJson(allData);
            writer.write(jsonRoot.toString());
            writer.close();
        } catch (JSONException je) {
            throw new IOException("Error create JSON", je);
        }

        final Map<String, byte[]> fileMap = new HashMap<>();
        if (allData.getMetaData() == null) {
            final String dateFileNamePart = buildFileNamePart(exportDateTime);
            fileMap.put(buildFileName(dateFileNamePart), outputStream.toByteArray());
        } else {
            fileMap.put(BACKUP_FILENAME, outputStream.toByteArray());
        }
        return fileMap;
    }

    public AllData restoreFromImportData(String rawJson) throws IOException {
        try {
           return fromJson(rawJson);
        } catch (JSONException je) {
            throw new IOException("Error reading JSON", je);
        }
    }

    @VisibleForTesting
    JSONObject buildJson(AllData allData) throws JSONException {
        final JSONObject jsonRoot = new JSONObject();
        if (allData.getMetaData() != null) {
            final JSONObject metadata = formatMetadata(allData.getMetaData());
            jsonRoot.put(JSON_KEY_METADATA, metadata);
        }
        final JSONArray addresses = new JSONArray();
        for (Address address : allData.getAddressList()) {
            addresses.put(formatAddress(address));
        }
        jsonRoot.put(JSON_KEY_ADDRESSES, addresses);
        final JSONArray addressChecks = new JSONArray();
        for (AddressCheck addressCheck : allData.getAddressCheckList()) {
            addressChecks.put(formatAddressCheck(addressCheck));
        }
        jsonRoot.put(JSON_KEY_ADDRESS_CHECKS, addressChecks);
        final JSONArray xchangeRates = new JSONArray();
        for (XChangeRate xChangeRate : allData.getXChangeRateList()) {
            xchangeRates.put(formatXChangeRate(xChangeRate));
        }
        jsonRoot.put(JSON_KEY_XCHANGE_RATES, xchangeRates);
        return jsonRoot;
    }

    @VisibleForTesting
    JSONObject formatAddress(Address address) throws JSONException {
        return new JSONObject()
                .put("btcAddress", address.btcAddress)
                .put("customName", address.customName)
                .put("addressType", address.addressType)
                .put("addressCheckId", address.addressCheckId)
                .put("createdAt", dateToString(address.createdAt));
    }

    @VisibleForTesting
    Address readAddress(JSONObject jsonObject) throws JSONException {
        final Address address = new Address();
        address.btcAddress = jsonObject.getString("btcAddress");
        if (jsonObject.has("customName")) {
           address.customName = jsonObject.getString("customName");
        }
        if (jsonObject.has("addressType")) {
            address.addressType = jsonObject.getString("addressType");
        }
        address.addressCheckId = jsonObject.getLong("addressCheckId");
        if (jsonObject.has("createdAt")) {
            address.createdAt = stringToDate(jsonObject.getString("createdAt"));
        }
        return address;
    }

    @VisibleForTesting
    JSONObject formatAddressCheck(AddressCheck addressCheck) throws JSONException {
        return new JSONObject()
                .put("id", addressCheck.id)
                .put("btcAddress", addressCheck.btcAddress)
                .put("btcAmount", bigDecimalToString(addressCheck.btcAmount))
                .put("numTransactions", addressCheck.numTransactions)
                .put("numFundedTxo", addressCheck.numFundedTxo)
                .put("numSpentTxo", addressCheck.numSpentTxo)
                .put("checkedAt", dateToString(addressCheck.checkedAt));
    }

    @VisibleForTesting
    AddressCheck readAddressCheck(JSONObject jsonObject) throws JSONException {
        final AddressCheck addressCheck = new AddressCheck();
        addressCheck.id = jsonObject.getLong("id");
        addressCheck.btcAddress = jsonObject.getString("btcAddress");
        addressCheck.btcAmount = stringToBigDecimal(jsonObject.getString("btcAmount"));
        addressCheck.numTransactions = jsonObject.getInt("numTransactions");
        addressCheck.numFundedTxo = jsonObject.getInt("numFundedTxo");
        addressCheck.numSpentTxo = jsonObject.getInt("numSpentTxo");
        addressCheck.rawResult = "{}"; // not in backup
        addressCheck.checkedAt = stringToDate(jsonObject.getString("checkedAt"));
        return addressCheck;
    }

    @VisibleForTesting
    JSONObject formatXChangeRate(XChangeRate xChangeRate) throws JSONException {
        return new JSONObject()
                .put("id", xChangeRate.id)
                .put("btcEuroRate", bigDecimalToString(xChangeRate.btcEuroRate))
                .put("btcDollarRate", bigDecimalToString(xChangeRate.btcDollarRate))
                .put("checkedAt", dateToString(xChangeRate.checkedAt));
    }

    @VisibleForTesting
    XChangeRate readXChangeRate(JSONObject jsonObject) throws JSONException {
        final XChangeRate xChangeRate = new XChangeRate();
        xChangeRate.id = jsonObject.getLong("id");
        if (jsonObject.has("btcEuroRate")) {
            xChangeRate.btcEuroRate = stringToBigDecimal(jsonObject.getString("btcEuroRate"));
        }
        if (jsonObject.has("btcDollarRate")) {
            xChangeRate.btcDollarRate = stringToBigDecimal(jsonObject.getString("btcDollarRate"));
        }
        if (jsonObject.has("checkedAt")) {
            xChangeRate.checkedAt = stringToDate(jsonObject.getString("checkedAt"));
        }
        return xChangeRate;
    }

    @VisibleForTesting
    JSONObject formatMetadata(Map<String, Object> metaData) throws JSONException {
        JSONObject metadataObject = new JSONObject();
        for(String key : metaData.keySet()) {
            Object data = metaData.get(key);
            String payload;
            if (data instanceof Date) {
                payload = dateToString((Date)data);
            } else {
                payload = data.toString();
            }
            metadataObject = metadataObject.put(key, payload);
        }
        return metadataObject;
    }

    @VisibleForTesting
    AllData fromJson(String rawJson) throws JSONException {
        JSONObject rootObject = new JSONObject(rawJson);
        List<Address> addressList = new ArrayList<>();
        JSONArray jsonAddressList = rootObject.getJSONArray(JSON_KEY_ADDRESSES);
        for (int i = 0; i < jsonAddressList.length(); ++i) {
            addressList.add(readAddress(jsonAddressList.getJSONObject(i)));
        }
        List<AddressCheck> addressCheckList = new ArrayList<>();
        JSONArray jsonAddressCheckList = rootObject.getJSONArray(JSON_KEY_ADDRESS_CHECKS);
        for (int i = 0; i < jsonAddressCheckList.length(); ++i) {
            addressCheckList.add(readAddressCheck(jsonAddressCheckList.getJSONObject(i)));
        }
        List<XChangeRate> xChangeRateList = new ArrayList<>();
        JSONArray jsonXChangeRateList = rootObject.getJSONArray(JSON_KEY_XCHANGE_RATES);
        for (int i = 0; i < jsonXChangeRateList.length(); ++i) {
            xChangeRateList.add(readXChangeRate(jsonXChangeRateList.getJSONObject(i)));
        }
        return new AllData(addressList, addressCheckList, xChangeRateList);
    }

    private String bigDecimalToString(BigDecimal bigDecimal) {
        return bigDecimal == null ? "" : bigDecimal.toPlainString();
    }

    private BigDecimal stringToBigDecimal(String bigDecimalStr) {
        if (bigDecimalStr == null || bigDecimalStr.isEmpty()) {
            return null;
        }
        return new BigDecimal(bigDecimalStr);
    }

    private String dateToString(Date date) {
        if (date == null) {
            return "";
        }
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.of("UTC"));
        return zonedDateTime.format(DATE_FORMATTER);
    }

    private Date stringToDate(String dateStr) {
        if (dateStr == null || dateStr.isEmpty()) {
            return null;
        }
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateStr, DATE_FORMATTER);
        return Date.from(zonedDateTime.toInstant());
    }

    private String buildFileNamePart(Date exportDateTime) {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm");
        final ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(exportDateTime.toInstant(), ZoneId.of("UTC"));
        return zonedDateTime.format(dateTimeFormatter);
    }

    private String buildFileName(String dateFileNamePart) {
        return String.format("%s.%s", dateFileNamePart, FILE_EXTENSION);
    }
}
