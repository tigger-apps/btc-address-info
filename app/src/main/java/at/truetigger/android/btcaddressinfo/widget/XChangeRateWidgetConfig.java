package at.truetigger.android.btcaddressinfo.widget;

public class XChangeRateWidgetConfig {

    private final WidgetCurrency currency;
    private final WidgetTheme theme;

    public XChangeRateWidgetConfig(WidgetCurrency currency, WidgetTheme theme) {
        this.currency = currency;
        this.theme = theme;
    }

    public WidgetCurrency getCurrency() {
        return currency;
    }

    public WidgetTheme getTheme() {
        return theme;
    }
}
