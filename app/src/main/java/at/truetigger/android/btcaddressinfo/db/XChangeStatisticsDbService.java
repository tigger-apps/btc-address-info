package at.truetigger.android.btcaddressinfo.db;

import java.time.Duration;
import java.util.Date;
import java.util.List;

import at.truetigger.android.btcaddressinfo.ServiceCallback;
import at.truetigger.android.btcaddressinfo.db.dto.XChangeStatistics;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;
import at.truetigger.android.btcaddressinfo.util.DurationToDateConverter;

public class XChangeStatisticsDbService extends AbstractDbService {

    public static final int ERRO_CODE_STATISTICS_ERROR = 4001;

    public XChangeStatisticsDbService(AppDatabase database) {
        super(database);
    }

    public void gatherStatisticsForDuration(XChangeStatisticsDbService.XChangeStatisticsServiceCallback callback,
                                            int requestCode, Duration duration) {
        Runnable gatherStatisticsThread = () -> {
            try {
                Date statisticStartTimestamp = DurationToDateConverter.dateMinusDuration(new Date(), duration);
                List<XChangeRate> xChangeRates = getXChangeRateDao().findByMinimumCheckedAt(statisticStartTimestamp);
                XChangeStatistics xChangeStatistics = new XChangeStatistics(xChangeRates);
                onServiceSuccess(callback, requestCode, xChangeStatistics);
            } catch (Exception exception) {
                onServiceError(callback, requestCode, ERRO_CODE_STATISTICS_ERROR, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(gatherStatisticsThread).start();
    }

    private void onServiceSuccess(XChangeStatisticsDbService.XChangeStatisticsServiceCallback callback, int requestCode, XChangeStatistics xChangeStatistics) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, xChangeStatistics);
        });
    }

    public interface XChangeStatisticsServiceCallback extends ServiceCallback {
        void onServiceSuccess(int requestCode, XChangeStatistics xChangeStatistics);
    }
}
