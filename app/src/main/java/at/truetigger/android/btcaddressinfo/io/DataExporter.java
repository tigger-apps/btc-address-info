package at.truetigger.android.btcaddressinfo.io;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.db.dto.AllData;

public interface DataExporter {

    String getContentType();

    Map<String, byte[]> buildExportData(Date exportDateTime, AllData allData) throws IOException;

}
