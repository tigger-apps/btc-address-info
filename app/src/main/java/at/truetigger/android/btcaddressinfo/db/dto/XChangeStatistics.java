package at.truetigger.android.btcaddressinfo.db.dto;

import androidx.annotation.NonNull;

import java.math.BigDecimal;
import java.util.List;

import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

public class XChangeStatistics {

    private final XChangeRate firstRate;

    private final XChangeRate lastRate;

    private final XChangeRate highestRate;

    private final XChangeRate lowestRate;

    private final XChangeRate averageRate;

    private final int numRates;

    public XChangeStatistics(@NonNull List<XChangeRate> rates) {
        numRates = rates.size();
        if (numRates == 0) {
            firstRate = null;
            lastRate = null;
            highestRate = null;
            lowestRate = null;
            averageRate = null;
        } else {
            firstRate = rates.get(0);
            lastRate = rates.get(numRates - 1);
            highestRate = findHighestRate(rates);
            lowestRate = findLowestRate(rates);
            averageRate = calcAverageRate(rates);
        }
    }

    public XChangeRate getFirstRate() {
        return firstRate;
    }

    public XChangeRate getLastRate() {
        return lastRate;
    }

    public XChangeRate getHighestRate() {
        return highestRate;
    }

    public XChangeRate getLowestRate() {
        return lowestRate;
    }

    public XChangeRate getAverageRate() {
        return averageRate;
    }

    public int getNumRates() {
        return numRates;
    }

    private XChangeRate calcAverageRate(@NonNull List<XChangeRate> rates) {
        double sumEuro = 0d;
        double sumDollar = 0d;
        for (XChangeRate xChangeRate : rates) {
            sumEuro += xChangeRate.btcEuroRate.doubleValue();
            sumDollar += xChangeRate.btcDollarRate.doubleValue();
        }
        BigDecimal averageEuroRate = new BigDecimal(sumEuro / rates.size());
        BigDecimal averageDollarRate = new BigDecimal(sumDollar / rates.size());
        return XChangeRate.forEuroAndDollar(averageEuroRate, averageDollarRate);
    }

    private XChangeRate findHighestRate(@NonNull List<XChangeRate> rates) {
        XChangeRate currentMatch = null;
        for (XChangeRate xChangeRate : rates) {
            if (currentMatch == null || currentMatch.btcDollarRate.compareTo(xChangeRate.btcDollarRate) == -1) {
                currentMatch = xChangeRate;
            }
        }
        return currentMatch;
    }

    private XChangeRate findLowestRate(@NonNull List<XChangeRate> rates) {
        XChangeRate currentMatch = null;
        for (XChangeRate xChangeRate : rates) {
            if (currentMatch == null || currentMatch.btcDollarRate.compareTo(xChangeRate.btcDollarRate) == 1) {
                currentMatch = xChangeRate;
            }
        }
        return currentMatch;
    }
}
