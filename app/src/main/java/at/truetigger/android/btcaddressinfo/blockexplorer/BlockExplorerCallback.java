package at.truetigger.android.btcaddressinfo.blockexplorer;

import at.truetigger.android.btcaddressinfo.ServiceCallback;

public interface BlockExplorerCallback extends ServiceCallback {
    void onServiceSuccess(int requestCode, BtcAddressInfo btcAddressInfo);
}
