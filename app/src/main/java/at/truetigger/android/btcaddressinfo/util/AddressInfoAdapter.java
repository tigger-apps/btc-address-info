package at.truetigger.android.btcaddressinfo.util;

import android.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;

import at.truetigger.android.btcaddressinfo.AddressInfo;
import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.activity.MainActivity;

public class AddressInfoAdapter extends ArrayAdapter<AddressInfo> implements
        View.OnClickListener, View.OnLongClickListener {

    private static final String TAG = "AddressInfoAdapter";

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    private static final int VIEW_TAG_VIEWHOLDER = R.id.customName;
    private static final int VIEW_TAG_POSITION = R.id.btcAddress;

    private final MainActivity mainActivity;

    public AddressInfoAdapter(ArrayList<AddressInfo> addressInfoList, MainActivity mainActivity) {
        super(mainActivity, R.layout.listentry_address, addressInfoList);
        this.mainActivity = mainActivity;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        AddressInfo addressInfo = getItem(position);
        ViewHolder viewHolder;

        if (view == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.listentry_address, parent, false);
            viewHolder.customNameTextView = view.findViewById(R.id.customName);
            viewHolder.btcAddressTextView = view.findViewById(R.id.btcAddress);
            viewHolder.lastAmountTextView = view.findViewById(R.id.lastAmount);
            viewHolder.lastCheckedTextView = view.findViewById(R.id.lastChecked);
            view.setTag(VIEW_TAG_VIEWHOLDER, viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag(VIEW_TAG_VIEWHOLDER);
        }

        viewHolder.customNameTextView.setText(addressInfo.getCustomName().orElse(""));
        viewHolder.btcAddressTextView.setText(addressInfo.getBtcAddress());
        viewHolder.lastAmountTextView.setText(BtcFormatter.formatCrypto(addressInfo.getLastAmount()));
        viewHolder.lastCheckedTextView.setText(DATE_TIME_FORMATTER.format(Instant.ofEpochMilli(addressInfo.getLastChecked().getTime()).atZone(ZoneId.systemDefault())));

        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        view.setTag(VIEW_TAG_POSITION, position);
        return view;
    }

    @Override
    public void onClick(View clickedView) {
        final int position = (Integer) clickedView.getTag(VIEW_TAG_POSITION);
        final AddressInfo addressInfo = getItem(position);

        mainActivity.processAddress(addressInfo.getBtcAddress(), true);
    }

    @Override
    public boolean onLongClick(View longClickedView) {
        final int position = (Integer) longClickedView.getTag(VIEW_TAG_POSITION);
        final AddressInfo addressInfo = getItem(position);

        final EditText nameField = new EditText(mainActivity);
        nameField.setText(addressInfo.getCustomName().orElse(""));
        final AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setTitle(R.string.dialog_title_rename)
                .setView(nameField)
                .setPositiveButton(R.string.dialog_ok, (dialog, itemPosition) -> mainActivity.updateCustomName(addressInfo.getBtcAddress(), nameField.getText().toString()))
                .setNegativeButton(R.string.dialog_cancel, (dialog, itemPosition) -> Log.d(TAG, "Cancel"));
        AlertDialog dialog  = builder.create();
        dialog.show();
        // TODO Auto-generated method stub
        return true;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView customNameTextView;
        TextView btcAddressTextView;
        TextView lastAmountTextView;
        TextView lastCheckedTextView;
    }
}
