package at.truetigger.android.btcaddressinfo.db;

import java.util.ArrayList;
import java.util.List;

import at.truetigger.android.btcaddressinfo.AddressInfo;
import at.truetigger.android.btcaddressinfo.ServiceCallback;
import at.truetigger.android.btcaddressinfo.blockexplorer.BtcAddressInfo;
import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;

public class AddressDbService extends AbstractDbService {

    public static final int ERR_CODE_CANNOT_STORE = 2001;
    public static final int ERR_CODE_FIND_ALL = 2002;
    public static final int ERR_CODE_FETCH_DETAIL = 2003;
    public static final int ERR_CODE_DELETE = 2004;
    public static final int ERR_CODE_UPDATE_CUSTOMNAME = 2005;

    public AddressDbService(AppDatabase database) {
        super(database);
    }

    public void storeAddress(BtcAddressInfo addressInfo, AddressDbServiceCallback callback, int requestCode) {
        Runnable storeAddressThread = () -> {
            try {
                Long addressCheckId = getAddressCheckDao().insert(AddressCheck.forBtcAddressInfo(addressInfo));
                if (addressCheckId == null) {
                    onServiceError(callback, requestCode, ERR_CODE_CANNOT_STORE, "Cannot store addressCheck");
                    return;
                }
                Address storedAddress = getAddressDao().findByBtcAddress(addressInfo.getAddress());
                if (storedAddress == null) {
                    Address newAddress = Address.forBtcAddress(addressInfo.getAddress());
                    newAddress.addressCheckId = addressCheckId;
                    getAddressDao().insert(newAddress);
                    onServiceSuccess(callback, requestCode, newAddress);
                } else {
                    storedAddress.addressCheckId = addressCheckId;
                    getAddressDao().update(storedAddress);
                    onServiceSuccess(callback, requestCode, storedAddress);
                }
            } catch (Exception exception) {
                onServiceError(callback, requestCode, ERR_CODE_CANNOT_STORE, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(storeAddressThread).start();
    }

    public void getAllAddresses(AddressDbServiceCallback callback, int requestCode) {
        Runnable getAllAddressesThread = () -> {
            try {
                List<AddressInfo> allAddressInfo = new ArrayList<>();
                getAddressDao().findAll().forEach(address -> {
                    AddressCheck addressCheck = getAddressCheckDao().findById(address.addressCheckId);
                    allAddressInfo.add(new AddressInfo(address.customName, address.btcAddress, addressCheck.btcAmount,  addressCheck.checkedAt));
                });
                onServiceSuccess(callback, requestCode, allAddressInfo);
            } catch (Exception exception) {
                onServiceError(callback, requestCode, ERR_CODE_FIND_ALL, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(getAllAddressesThread).start();
    }

    public void getAddressDetails(String btcAddress, AddressDbServiceCallback callback, int requestCode) {
        Runnable getAddressDetailThread = () -> {
            try {
                Address address = getAddressDao().findByBtcAddress(btcAddress);
                if (address == null) {
                    onServiceError(callback, requestCode, ERR_CODE_FETCH_DETAIL, "Cannot find record for address " + btcAddress);
                    return;
                }
                AddressCheck lastAddressCheck = getAddressCheckDao().findById(address.addressCheckId);
                if (lastAddressCheck == null) {
                    onServiceError(callback, requestCode, ERR_CODE_FETCH_DETAIL, "Cannot find last addressCheckResult for address " + btcAddress);
                    return;
                }
                onServiceSuccess(callback, requestCode, address, lastAddressCheck);
            } catch (Exception exception) {
                onServiceError(callback, requestCode, ERR_CODE_FETCH_DETAIL, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(getAddressDetailThread).start();
    }

    public void removeAddressDetails(String btcAddress, AddressDbServiceCallback callback, int requestCode) {
        Runnable deleteAddressThread = () -> {
            try {
                Address address = Address.forBtcAddress(btcAddress);
                getAddressDao().delete(address);
                onServiceSuccess(callback, requestCode);
            } catch (Exception exception) {
                onServiceError(callback, requestCode, ERR_CODE_DELETE, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(deleteAddressThread).start();
    }

    public void updateCustomName(String btcAddress, String newCustomName, AddressDbServiceCallback callback, int requestCode) {
        Runnable updateThread = () -> {
            try {
                Address address =  getAddressDao().findByBtcAddress(btcAddress);
                address.customName = newCustomName;
                getAddressDao().update(address);
                onServiceSuccess(callback, requestCode);
            } catch (Exception exception) {
                onServiceError(callback, requestCode, ERR_CODE_UPDATE_CUSTOMNAME, exception.getClass().getCanonicalName() + ": " + exception.getMessage());
            }
        };
        new Thread(updateThread).start();
    }

    private void onServiceSuccess(AddressDbServiceCallback callback, int requestCode, Address address) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, address);
        });
    }

    private void onServiceSuccess(AddressDbServiceCallback callback, int requestCode, List<AddressInfo> addressInfoList) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, addressInfoList);
        });
    }

    private void onServiceSuccess(AddressDbServiceCallback callback, int requestCode, Address address, AddressCheck addressCheck) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, address, addressCheck);
        });
    }

    public interface AddressDbServiceCallback extends ServiceCallback {
        void onServiceSuccess(int requestCode, Address address);
        void onServiceSuccess(int requestCode, Address address, AddressCheck addressCheck);
        void onServiceSuccess(int requestCode, List<AddressInfo> addressList);
    }
}
