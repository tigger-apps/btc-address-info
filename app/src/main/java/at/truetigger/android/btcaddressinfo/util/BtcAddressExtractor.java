package at.truetigger.android.btcaddressinfo.util;

public class BtcAddressExtractor {

    private static final String BASE58_PATTERN = "[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{26,35}";
    private static final String BECH32_PATTERN = "bc1[qpzry9x8gf2tvdw0s3jn54khce6mua7l]{11,71}";

    public String extractBtcAddress(String input) {

        if (input == null) {
            return null;
        }

        String withoutPrefix = stripPrefix(input);
        String withoutSuffix = stripSuffix(withoutPrefix);

        if (withoutSuffix.matches(BASE58_PATTERN)) {
            char firstChar = withoutSuffix.charAt(0);
            if(firstChar == '1' || firstChar == '3') {
                return withoutSuffix;
            }
        }

        String lowercased = withoutSuffix.toLowerCase();
        if (lowercased.matches(BECH32_PATTERN)) {
           return lowercased;
        }

        return null;
    }

    private String stripPrefix(String text) {
        int separator = text.indexOf(':');
        if (separator == -1) {
            return text;
        } else if (separator == text.length() -1) {
            return "";
        } else {
            return text.substring(separator + 1);
        }
    }

    private String stripSuffix(String text) {
        int separator = text.indexOf('?');
        if (separator == -1) {
            return text;
        } else {
            return text.substring(0, separator);
        }
    }
}
