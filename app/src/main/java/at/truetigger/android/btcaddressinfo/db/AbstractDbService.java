package at.truetigger.android.btcaddressinfo.db;

import android.os.Handler;
import android.os.Looper;

import at.truetigger.android.btcaddressinfo.ServiceCallback;
import at.truetigger.android.btcaddressinfo.db.dao.AddressCheckDao;
import at.truetigger.android.btcaddressinfo.db.dao.AddressDao;
import at.truetigger.android.btcaddressinfo.db.dao.XChangeRateDao;

public abstract class AbstractDbService {

    private final AppDatabase database;
    private final Handler handler;

    public AbstractDbService(AppDatabase database) {
        this.database = database;
        this.handler = new Handler(Looper.getMainLooper());
    }

    protected AddressCheckDao getAddressCheckDao() {
        return this.database.getAddressCheckDao();
    }

    protected AddressDao getAddressDao() {
        return this.database.getAddressDao();
    }

    protected XChangeRateDao getXChangeRateDao() {
        return this.database.getXChangeRateDao();
    }

    protected void clearAllTables() {
        database.clearAllTables();
    }

    protected void runOnMainThread(Runnable runnable) {
        handler.post(runnable);
    }

    protected void onServiceSuccess(ServiceCallback callback, int requestCode) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode);
        });
    }

    protected void onServiceError(ServiceCallback callback, int requestCode, int errorCode, String errorMessage) {
        runOnMainThread(() -> {
            callback.onServiceError(requestCode, errorCode, errorMessage);
        });
    }
}
