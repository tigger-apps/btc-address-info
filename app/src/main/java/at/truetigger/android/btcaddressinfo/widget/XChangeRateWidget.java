package at.truetigger.android.btcaddressinfo.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.content.ContextCompat;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.activity.ConfigureXChangeRateWidget;
import at.truetigger.android.btcaddressinfo.db.XChangeRateDbService;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;
import at.truetigger.android.btcaddressinfo.service.RefreshRatesJobService;
import at.truetigger.android.btcaddressinfo.util.FiatFormatter;

public class XChangeRateWidget extends AppWidgetProvider implements
        XChangeRateDbService.XChangeRateServiceCallback {

    private static final String TAG = "XChangeRateWidget";

    public static final String RECONFIGURE_ACTION = "at.truetigger.btcaddressinfo.APPWIDGET_RECONFIGURE";

    private static final int RC_DB_REQUEST = 901;
    private static final String KEY_WIDGET_ID = "widgetId";
    private static final String KEY_CURRENCY = "currency";
    private static final String KEY_THEME = "theme";

    private Context context;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d(TAG, "Updating widgets: " + Arrays.toString(appWidgetIds));
        for (int appWidgetId : appWidgetIds) {
            updateWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getExtras() != null) {
            final int appWidgetId = intent.getExtras().getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);

            if (intent.getAction().equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                updateWidget(context, appWidgetManager, appWidgetId);
                Log.d(TAG, "WidgetUpdateIntent received: " + intent);

            } else if(intent.getAction().equals(RECONFIGURE_ACTION)) {
                Log.d(TAG, "starting reconfigure for widget #" + appWidgetId);
                Intent intentConfig = new Intent(context, ConfigureXChangeRateWidget.class);
                intentConfig.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intentConfig.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
                intentConfig.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[]{appWidgetId});
                intentConfig.putExtra(ConfigureXChangeRateWidget.RECONFIGURE_WIDGET, true);
                context.startActivity(intentConfig);
            }
        }
    }

    @Override
    public void onServiceSuccess(int requestCode, Map<String, Object> metaData, XChangeRate xChangeRate) {
        Log.d(TAG, "Update xChangeRate " + xChangeRate.btcDollarRate + " (" + xChangeRate.checkedAt + ")");

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_xchange_rate);
        remoteViews.setInt(R.id.widget_errMsg, "setVisibility", View.GONE);

        WidgetCurrency widgetCurrency = getWidgetCurrencyFromMetaData(metaData);
        String fiat = FiatFormatter.formatFiat(widgetCurrency.getSymbol(), widgetCurrency == WidgetCurrency.USD ? xChangeRate.btcDollarRate : xChangeRate.btcEuroRate);
        remoteViews.setTextViewText(R.id.widget_out_xchangeRate, fiat);
        remoteViews.setTextViewText(R.id.widget_out_lastUpdated, formatDate(xChangeRate.checkedAt));
        remoteViews.setInt(R.id.widget_out_lastUpdated, "setVisibility", View.VISIBLE);
        updateWidgetByMetadata(metaData, remoteViews);
    }

    @Override
    public void onServiceError(int requestCode, Map<String, Object> metaData, int errorCode, String errorMessage) {
        Log.e(TAG, "Error fetching last xchange rate from DB: " + errorCode + " (" + errorMessage + ")");

        String errMsg = (XChangeRateDbService.ERR_CODE_CANNOT_FIND_LATEST_ENTRY == errorCode ? "No rates available" : "Error access DB");
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_xchange_rate);
        remoteViews.setInt(R.id.widget_out_lastUpdated, "setVisibility", View.GONE);
        remoteViews.setTextViewText(R.id.widget_errMsg, errMsg);
        remoteViews.setInt(R.id.widget_errMsg, "setVisibility", View.VISIBLE);
        remoteViews.setTextViewText(R.id.widget_out_xchangeRate, "---");

        updateWidgetByMetadata(metaData, remoteViews);
    }

    @Override
    public void onServiceSuccess(int requestCode, XChangeRate xChangeRate) {
        Log.e(TAG, "unexpected onServiceSuccess without context");
    }

    @Override
    public void onServiceSuccess(int requestCode) {
        Log.e(TAG, "unexpected onServiceSuccess without context");
    }

    @Override
    public void onServiceError(int requestCode, int errorCode, String errorMessage) {
        Log.e(TAG, "unexpected onServiceError without context, errCode=" + errorCode + " (" + errorMessage + ")");
    }

    private void updateWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        this.context = context;
        if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_xchange_rate);

            // update widget settings from current config
            XChangeRateWidgetConfig config = loadWidgetConfig(context, appWidgetId);
            Log.d(TAG, "Config read for widget[" + appWidgetId + "] with properties: " + config.getCurrency() + "/" + config.getTheme());

            // trigger configuration for specific widget
            Intent intentConfig = new Intent(context, XChangeRateWidget.class);
            intentConfig.setAction(RECONFIGURE_ACTION);
            intentConfig.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            PendingIntent callConfigIntent = PendingIntent.getBroadcast(context, appWidgetId, intentConfig, PendingIntent.FLAG_CANCEL_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.widget_out_xchangeRate, callConfigIntent);

            // trigger update
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
            Log.d(TAG, "Store settings for widget " + appWidgetId);

            // fetch latest rate from DB
            XChangeRateDbService xChangeRateDbService = ((MyApplication)context.getApplicationContext()).getXChangeRateDbService();
            Map<String, Object> metaData = new HashMap<>();
            metaData.put(KEY_WIDGET_ID, appWidgetId);
            metaData.put(KEY_CURRENCY, config.getCurrency());
            metaData.put(KEY_THEME, config.getTheme());
            xChangeRateDbService.findLatestXchangeRate(this, RC_DB_REQUEST, metaData);
        }
    }

    private void updateWidgetByMetadata(Map<String, Object> metaData, RemoteViews remoteViews) {
        int appWidgetId = getWidgetIdFromMetaData(metaData);
        if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
            WidgetTheme theme = getWidgetThemeFromMetaData(metaData);
            int textColor = ContextCompat.getColor(context, theme == WidgetTheme.DARK ? R.color.dark : R.color.light);
            remoteViews.setInt(R.id.widget_out_xchangeRate, "setTextColor",textColor);
            remoteViews.setInt(R.id.widget_out_lastUpdated, "setTextColor", textColor);
            AppWidgetManager.getInstance(context).updateAppWidget(appWidgetId, remoteViews);
        }
    }

    private int getWidgetIdFromMetaData(Map<String, Object> metaData) {
        return (metaData != null && metaData.containsKey(KEY_WIDGET_ID))
                ? (Integer) metaData.get(KEY_WIDGET_ID)
                : AppWidgetManager.INVALID_APPWIDGET_ID;
    }

    private WidgetCurrency getWidgetCurrencyFromMetaData(Map<String, Object> metaData) {
        return (metaData != null && metaData.containsKey(KEY_CURRENCY) && metaData.get(KEY_CURRENCY) == WidgetCurrency.EUR)
                ? WidgetCurrency.EUR
                : WidgetCurrency.USD;
    }

    private WidgetTheme getWidgetThemeFromMetaData(Map<String, Object> metaData) {
        return (metaData != null && metaData.containsKey(KEY_THEME) && metaData.get(KEY_THEME) == WidgetTheme.DARK)
                ? WidgetTheme.DARK
                : WidgetTheme.LIGHT;
    }

    private XChangeRateWidgetConfig loadWidgetConfig(Context context, int appWidgetId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MyApplication.PREFERENCES_NAME, Context.MODE_PRIVATE);
        final WidgetCurrency currency = WidgetCurrency.valueOf(sharedPreferences.getString(ConfigureXChangeRateWidget.RATE_WIDGET_CURRENCY_SETTING_KEY + appWidgetId, WidgetCurrency.USD.name()));
        final WidgetTheme theme = WidgetTheme.valueOf(sharedPreferences.getString(ConfigureXChangeRateWidget.RATE_WIDGET_THEME_SETTING_KEY + appWidgetId, WidgetTheme.LIGHT.name()));
        return new XChangeRateWidgetConfig(currency, theme);
    }

    private String formatDate(Date date) {
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        return dateFormat.format(date);
    }
}
