package at.truetigger.android.btcaddressinfo.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverters;

import java.util.Date;
import java.util.List;

import at.truetigger.android.btcaddressinfo.db.converter.TimestampConverter;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

@Dao
public interface XChangeRateDao {

    @Insert
    Long insert(XChangeRate xChangeRate);

    @Query("SELECT * FROM xchange_rate WHERE checked_at = (SELECT MAX(checked_at) FROM xchange_rate)")
    XChangeRate findLatestXChangeRate();

    @Query("SELECT * FROM xchange_rate ORDER BY checked_at ASC")
    List<XChangeRate> findAll();

    @Query("SELECT * FROM xchange_rate WHERE checked_at >= :minCheckedAt ORDER BY checked_at ASC")
    @TypeConverters(TimestampConverter.class)
    List<XChangeRate> findByMinimumCheckedAt(Date minCheckedAt);
}
