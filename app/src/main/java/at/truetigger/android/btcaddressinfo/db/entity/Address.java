package at.truetigger.android.btcaddressinfo.db.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import at.truetigger.android.btcaddressinfo.db.converter.TimestampConverter;

@Entity(tableName = "address",
        indices = {@Index("address_check_id")},
        foreignKeys = @ForeignKey(entity= AddressCheck.class, parentColumns="id", childColumns="address_check_id")
)
@TypeConverters(TimestampConverter.class)
public class Address {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "btc_address")
    public String btcAddress;

    @ColumnInfo(name = "custom_name")
    public String customName;

    @ColumnInfo(name = "address_type")
    public String addressType;

    @ColumnInfo(name = "address_check_id")
    @NonNull
    public Long addressCheckId;

    @ColumnInfo(name = "created_at")
    public Date createdAt;

    public static Address forBtcAddress(String btcAddress) {
        Address address = new Address();
        address.btcAddress = btcAddress;
        address.createdAt = new Date();
        return address;
    }
}
