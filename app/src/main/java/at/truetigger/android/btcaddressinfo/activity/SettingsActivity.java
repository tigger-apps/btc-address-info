package at.truetigger.android.btcaddressinfo.activity;

import android.os.Bundle;
import android.widget.CheckBox;

import androidx.appcompat.app.AppCompatActivity;

import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.util.BackgroundControlService;

public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = "SettingsActivity";

    private CheckBox rateRefreshCheckbox;
    private BackgroundControlService backgroundControlService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initServices();
        bindComponents();
    }

    private void initServices() {
        backgroundControlService = ((MyApplication) getApplication()).getBackgroundControlService();
    }

    private void bindComponents() {
        rateRefreshCheckbox = findViewById(R.id.chkbox_xchange_rate_refresh);
        rateRefreshCheckbox.setChecked(backgroundControlService.isRateRefreshEnabled());
        rateRefreshCheckbox.setOnClickListener(v -> {
            boolean isChecked = ((CheckBox)v).isChecked();
            backgroundControlService.toggleRateRefresh(isChecked);
            if (isChecked) {
                backgroundControlService.startRateRefresh();
            } else {
                backgroundControlService.stopRateRefresh();
            }
        });
    }
}
