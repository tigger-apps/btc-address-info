package at.truetigger.android.btcaddressinfo.util;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.List;

import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.service.RefreshRatesJobService;

public class BackgroundControlService {

    private static final String TAG = "BackgroundControlService";

    private static final long MILLISECONDS_EACH_MINUTES = 60 * 1000;
    private static final int XCHANGE_REFRESH_JOB_ID = 23;

    // Rate refresh settings
    private static final String RATEREFRESH_SETTING_KEY = "RateRefreshEnabled";
    private static final int RATEREFRESH_INTERVAL_MINUTES = 15;
    private static final int RATEREFRESH_FLEX_MINUTES = 5;

    private Context context;
    private JobScheduler jobScheduler;

    public BackgroundControlService(Context context) {
        this.context = context;
        jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

    public boolean isRateRefreshEnabled() {
        SharedPreferences sharedpreferences = context.getSharedPreferences(MyApplication.PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedpreferences.getBoolean(RATEREFRESH_SETTING_KEY, false);
    }

    public void toggleRateRefresh(boolean enabled) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MyApplication.PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(RATEREFRESH_SETTING_KEY, enabled);
        editor.commit();
    }

    public void startRateRefresh() {
        if (!isRateRefreshAlreadyScheduled()) {
            scheduleRateRefresh();
        } else {
            Log.d(TAG, "XChange refresh background service is already running");
        }
    }

    public void stopRateRefresh() {
        jobScheduler.cancel(XCHANGE_REFRESH_JOB_ID);
        Log.d(TAG, "XChange refresh background service stopped");
    }

    private void scheduleRateRefresh() {
        JobInfo jobInfo = new JobInfo.Builder(XCHANGE_REFRESH_JOB_ID, new ComponentName(context, RefreshRatesJobService.class))
                .setPeriodic(RATEREFRESH_INTERVAL_MINUTES *  MILLISECONDS_EACH_MINUTES, RATEREFRESH_FLEX_MINUTES * MILLISECONDS_EACH_MINUTES)
                .build();
        jobScheduler.schedule(jobInfo);
        Log.d(TAG, "XChange refresh background service started");
    }

    private boolean isRateRefreshAlreadyScheduled() {
        return jobScheduler.getAllPendingJobs().stream()
                .anyMatch(jobInfo -> XCHANGE_REFRESH_JOB_ID == jobInfo.getId());
    }
}
