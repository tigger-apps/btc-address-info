package at.truetigger.android.btcaddressinfo.db.converter;

import androidx.room.TypeConverter;

import java.math.BigDecimal;

public class BigDecimalConverter {

    @TypeConverter
    public static BigDecimal toBigDecimal(String bigDecimalString) {
        return bigDecimalString == null ? null : new BigDecimal(bigDecimalString);
    }

    @TypeConverter
    public static String fromBigDecimal(BigDecimal bigDecimal) {
        return bigDecimal == null ? null : bigDecimal.toPlainString();
    }
}
