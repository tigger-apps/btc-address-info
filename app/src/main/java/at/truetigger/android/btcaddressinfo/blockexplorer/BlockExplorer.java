package at.truetigger.android.btcaddressinfo.blockexplorer;

public interface BlockExplorer {
    void requestBtcAddressInfo(String address, BlockExplorerCallback callback, int requestCode);
}
