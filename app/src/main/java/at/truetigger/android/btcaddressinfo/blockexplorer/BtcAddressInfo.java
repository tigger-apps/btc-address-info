package at.truetigger.android.btcaddressinfo.blockexplorer;

import java.math.BigDecimal;

public final class BtcAddressInfo {

    private String address;
    private int numTransactions;
    private int numFundedTXO;
    private int numSpentTXO;
    private BigDecimal sumAmount;
    private String rawResult;

    public String getAddress() {
        return address;
    }

    public int getNumTransactions() {
        return numTransactions;
    }

    public int getNumFundedTXO() {
        return numFundedTXO;
    }

    public int getNumSpentTXO() {
        return numSpentTXO;
    }

    public BigDecimal getSumAmount() {
        return sumAmount;
    }

    public String getRawResult() {
        return rawResult;
    }

    public static BtcAddressInfo from(String address, int numTransactions, int numFundedTXO, int numSpentTXO, BigDecimal sumAmount, String rawResult) {
        BtcAddressInfo btcAddressInfo = new BtcAddressInfo();
        btcAddressInfo.address = address;
        btcAddressInfo.numTransactions = numTransactions;
        btcAddressInfo.numFundedTXO = numFundedTXO;
        btcAddressInfo.numSpentTXO = numSpentTXO;
        btcAddressInfo.sumAmount = sumAmount;
        btcAddressInfo.rawResult = rawResult;
        return btcAddressInfo;
    }
}
