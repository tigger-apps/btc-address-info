package at.truetigger.android.btcaddressinfo.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BtcFormatter {

    private static final BigDecimal MBTC_MULTIPLIER = new BigDecimal("1000");
    private static final BigDecimal MBTC_LIMIT = new BigDecimal("0.1");
    private static final String BTC_LABEL = "BTC";
    private static final String MBTC_LABEL = "mBTC";
    private static final int SCALE = 3;

    public static String formatCrypto(BigDecimal btcAmount) {
        if (btcAmount.subtract(MBTC_LIMIT).compareTo(BigDecimal.ZERO) == -1) {
            return formatAsMbtc(btcAmount);
        }
        return formatAsBtc(btcAmount);
    }

    private static String formatAsMbtc(BigDecimal btcAmount) {
        return String.format("%s %s",
                btcAmount.multiply(MBTC_MULTIPLIER).setScale(SCALE, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString(),
                MBTC_LABEL);
    }

    private static String formatAsBtc(BigDecimal btcAmount) {
        return String.format("%s %s",
                btcAmount.setScale(SCALE, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString(),
                BTC_LABEL);
    }
}
