package at.truetigger.android.btcaddressinfo.xchangerateexplorer;

import androidx.annotation.VisibleForTesting;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class CoindeskXchangeRateExplorer implements XchangeRateExplorer {

    public static final int ERR_COINBASE_ERROR = 1501;
    public static final int ERR_UNEXPECTED_RESULT = 1502;

    private static final String COINDESK_BASE = "https://api.coindesk.com/v1/";
    private static final String CURRENT_PRICE = COINDESK_BASE + "bpi/currentprice.json";

    private static final String FIELD_BPI = "bpi";
    private static final String FIELD_USD = "USD";
    private static final String FIELD_EUR = "EUR";
    private static final String FIELD_RATE = "rate";

    @Override
    public void requestBtcRates(XchangeRateExplorerCallback callback, int requestCode) {
        AndroidNetworking.get(CURRENT_PRICE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onServiceSuccess(requestCode, toPriceInfo(response));
                        } catch (JSONException je) {
                            callback.onServiceError(requestCode, ERR_UNEXPECTED_RESULT, "Cannot interprete result: " + je.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        callback.onServiceError(requestCode, ERR_COINBASE_ERROR, "Network error: " + error.getMessage() + " / " + error.getErrorDetail());
                    }
                });
    }

    /*
    {
      "time":{
        "updated":"Mar 25, 2021 18:59:00 UTC",
        "updatedISO":"2021-03-25T18:59:00+00:00",
        "updateduk":"Mar 25, 2021 at 18:59 GMT"
      },
      "disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
      "chartName":"Bitcoin",
      "bpi":{
        "USD":{"code":"USD","symbol":"&#36;","rate":"52,300.4150","description":"United States Dollar","rate_float":52300.415},
        "GBP":{"code":"GBP","symbol":"&pound;","rate":"38,070.1520","description":"British Pound Sterling","rate_float":38070.152},
        "EUR":{"code":"EUR","symbol":"&euro;","rate":"44,418.3764","description":"Euro","rate_float":44418.3764}
      }
    }
    */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    Map<String, BigDecimal> toPriceInfo(JSONObject result) throws JSONException {
        Map<String, BigDecimal> map = new HashMap<>();
        final JSONObject bpiData = result.getJSONObject(FIELD_BPI);
        final JSONObject usdData = bpiData.getJSONObject(FIELD_USD);
        final JSONObject eurData = bpiData.getJSONObject(FIELD_EUR);
        map.put(CURRENCY_DOLLAR, coinbaseRateToBigDecimal(usdData.getString(FIELD_RATE)));
        map.put(CURRENCY_EURO, coinbaseRateToBigDecimal(eurData.getString(FIELD_RATE)));
        return map;
    }

    // NOTE: coinbase returns the correct rate with thousands separator which are not supported by BigDecimal. Remove all of these.
    private BigDecimal coinbaseRateToBigDecimal(String coinbaseRate) {
        return new BigDecimal(coinbaseRate.replaceAll(",", ""));
    }
}
