package at.truetigger.android.btcaddressinfo.io;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.ServiceCallback;
import at.truetigger.android.btcaddressinfo.db.dto.AllData;

public class ImportExportService {

    private static final String TAG = "ImportExportService";

    public static final int ERR_CODE_CANNOT_EXPORT = 4001;
    public static final int ERR_CODE_CANNOT_RESTORE = 4002;
    public static final int ERR_CODE_UNSUPPORTED_EXPORT_FORMAT = 4003;

    public static final String EXPORT_FORMAT_CSV = "CSV";
    public static final String EXPORT_FORMAT_JSON = "JSON";

    private static final String NOT_AVAILABLE = "N/A";

    private final Handler handler;

    public ImportExportService() {
        this.handler = new Handler(Looper.getMainLooper());
    }

    public void export(String exportFormat, AllData allData, ImportExportServiceCallback callback, int requestCode) {
        Runnable exportThread = () -> {
            DataExporter dataExporter = (EXPORT_FORMAT_CSV.equals(exportFormat) ? new CsvDataExporter() : (EXPORT_FORMAT_JSON.equals(exportFormat) ? new JsonDataExporter() : null));
            if (dataExporter == null) {
                callback.onServiceError(requestCode, ERR_CODE_UNSUPPORTED_EXPORT_FORMAT, "Unsupported format: " + exportFormat);
                return;
            }
            try {
                Map<String, byte[]> dataToExport = dataExporter.buildExportData(new Date(), allData);
                onServiceSuccess(callback, requestCode, dataExporter.getContentType(), dataToExport);
            } catch (IOException ioe) {
                callback.onServiceError(requestCode, ERR_CODE_CANNOT_EXPORT, ioe.getMessage());
            }
        };
        new Thread(exportThread).start();
    }

    public void backup(Context context, AllData allData, ImportExportServiceCallback callback, int requestCode) {
        Runnable writeBackupThread = () -> {
            AllData metaDataEnrichedData = new AllData(allData.getAddressList(), allData.getAddressCheckList(),
                                                       allData.getXChangeRateList(), buildMetaData(context));
            DataExporter dataExporter = new JsonDataExporter();
            try {
                Map<String, byte[]> backupData = dataExporter.buildExportData(new Date(), metaDataEnrichedData);
                onServiceSuccess(callback, requestCode, dataExporter.getContentType(), backupData);
            } catch (IOException ioe) {
                callback.onServiceError(requestCode, ERR_CODE_CANNOT_EXPORT, ioe.getMessage());
            }
        };
        new Thread(writeBackupThread).start();
    }

    public void restore(String rawJson, ImportExportServiceCallback callback, int requestCode) {
        Runnable restoreFromJsonThread = () -> {
            JsonDataExporter dataExporter = new JsonDataExporter();
            try {
                AllData allData = dataExporter.restoreFromImportData(rawJson);
                onServiceSuccess(callback, requestCode, allData);
            } catch (IOException ioe) {
                callback.onServiceError(requestCode, ERR_CODE_CANNOT_RESTORE, ioe.getMessage());
            }
        };
        new Thread(restoreFromJsonThread).start();
    }

    private Map<String, Object> buildMetaData(Context context) {
        Map<String, Object> metaData = new HashMap<>();
        metaData.put("backupDate", new Date());
        metaData.put("appVersion", getAppVersion(context));
        metaData.put("androidVersion", android.os.Build.VERSION.SDK_INT);
        return metaData;
    }

    private String getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException nnfe) {
            Log.e(TAG, "Cannot find package info for " + context.getPackageName());
            return NOT_AVAILABLE;
        }
    }

    private void onServiceSuccess(ImportExportServiceCallback callback, int requestCode,
                                  String contentType, Map<String, byte[]> dataToExport) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, contentType, dataToExport);
        });
    }

    private void onServiceSuccess(ImportExportServiceCallback callback, int requestCode, AllData allData) {
        runOnMainThread(() -> {
            callback.onServiceSuccess(requestCode, allData);
        });
    }

    private void runOnMainThread(Runnable runnable) {
        handler.post(runnable);
    }

    public interface ImportExportServiceCallback extends ServiceCallback {
        void onServiceSuccess(int requestCode, String contentType, Map<String, byte[]> fileData);
        void onServiceSuccess(int requestCode, AllData allData);
    }
}
