package at.truetigger.android.btcaddressinfo.blockexplorer;

import androidx.annotation.VisibleForTesting;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BlockstreamBlockExplorer implements BlockExplorer {

    public static final int ERR_BLOCKSTREAM_ERROR = 1001;
    public static final int ERR_UNEXPECTED_RESULT = 1002;

    private static final String BLOCKSTREAM_API_BASE = "https://blockstream.info/api";
    private static final String GET_ADDRESS = BLOCKSTREAM_API_BASE + "/address/{address}";

    private final static String FIELD_ADDRESS = "address";
    private final static String FIELD_CHAIN_STATS = "chain_stats";
    private final static String FIELD_MEMPOOL_STATS = "mempool_stats";
    private final static String FIELD_TX_COUNT = "tx_count";
    private final static String FIELD_FUNDED_TXO_COUNT = "funded_txo_count";
    private final static String FIELD_SPENT_TXO_COUNT = "spent_txo_count";
    private final static String FIELD_FUNDED_TXO_SUM = "funded_txo_sum";
    private final static String FIELD_SPENT_TXO_SUM = "spent_txo_sum";

    private final static BigDecimal BTC_DIVIDER = new BigDecimal("100000000");  // 1BTC = 100 Mio sats
    private final static int BTC_SCALE = 8;

    @Override
    public void requestBtcAddressInfo(String address, BlockExplorerCallback callback, int requestCode) {
        AndroidNetworking.get(GET_ADDRESS)
                .addPathParameter("address", address)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onServiceSuccess(requestCode, toBtcAddressInfo(response));
                        } catch (JSONException je) {
                            callback.onServiceError(requestCode, ERR_UNEXPECTED_RESULT, "Cannot interprete result: " + je.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        callback.onServiceError(requestCode, ERR_BLOCKSTREAM_ERROR, "Network error: " + error.getMessage() + " / " + error.getErrorDetail());
                    }
                });
    }

    /*
    {
	  "address":"BTCAddress",
	  "chain_stats": {
		"funded_txo_count":1,
		"funded_txo_sum":250000,
		"spent_txo_count":0,
		"spent_txo_sum":0,
		"tx_count":1
	  },
	  "mempool_stats":{
		"funded_txo_count":0,
		"funded_txo_sum":0,
		"spent_txo_count":0,
		"spent_txo_sum":0,
		"tx_count":0
  	  }
    }
    */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    BtcAddressInfo toBtcAddressInfo(JSONObject blockstreamAddressJson) throws JSONException {
        final String address = blockstreamAddressJson.getString(FIELD_ADDRESS);
        final JSONObject chainStats = blockstreamAddressJson.getJSONObject(FIELD_CHAIN_STATS);
        final JSONObject mempoolStats = blockstreamAddressJson.getJSONObject(FIELD_MEMPOOL_STATS);
        final int numTransactions = chainStats.getInt(FIELD_TX_COUNT) + mempoolStats.getInt(FIELD_TX_COUNT);
        final int numFundedTxo = chainStats.getInt(FIELD_FUNDED_TXO_COUNT) + mempoolStats.getInt(FIELD_FUNDED_TXO_COUNT);
        final int numSpentTxo = chainStats.getInt(FIELD_SPENT_TXO_COUNT) + mempoolStats.getInt(FIELD_SPENT_TXO_COUNT);
        final long fundedSatoshis = chainStats.getLong(FIELD_FUNDED_TXO_SUM) + mempoolStats.getLong(FIELD_FUNDED_TXO_SUM);
        final long spentSatoshis = chainStats.getLong(FIELD_SPENT_TXO_SUM) + mempoolStats.getLong(FIELD_SPENT_TXO_SUM);
        final long unspentSatoshis = fundedSatoshis - spentSatoshis;
        return BtcAddressInfo.from(address, numTransactions, numFundedTxo, numSpentTxo, satsToBtc(unspentSatoshis), blockstreamAddressJson.toString());
    }

    private BigDecimal satsToBtc(long numSats) {
        return BigDecimal.valueOf(numSats).divide(BTC_DIVIDER, BTC_SCALE, RoundingMode.UNNECESSARY);
    }
}
