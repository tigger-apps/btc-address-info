package at.truetigger.android.btcaddressinfo.util;

import androidx.annotation.NonNull;

import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

public class XChangeRateChangeCalculator {

    public static double changeInPercent(@NonNull XChangeRate oldRate, @NonNull XChangeRate newRate) {
        final double percentValue = newRate.btcDollarRate.doubleValue() * 100d / oldRate.btcDollarRate.doubleValue();
        return percentValue - 100d;
    }
}
