package at.truetigger.android.btcaddressinfo.activity;

import static at.truetigger.android.btcaddressinfo.util.XChangeStatisticsFragmentAdapter.TAB_ALL;
import static at.truetigger.android.btcaddressinfo.util.XChangeStatisticsFragmentAdapter.TAB_MONTH;
import static at.truetigger.android.btcaddressinfo.util.XChangeStatisticsFragmentAdapter.TAB_WEEK;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.text.DateFormat;
import java.time.Duration;

import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.db.XChangeStatisticsDbService;
import at.truetigger.android.btcaddressinfo.db.dto.XChangeStatistics;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;
import at.truetigger.android.btcaddressinfo.util.FiatFormatter;
import at.truetigger.android.btcaddressinfo.util.XChangeRateChangeCalculator;

public class XChangeStatisticsFragment extends Fragment implements
        XChangeStatisticsDbService.XChangeStatisticsServiceCallback {

    private static final String TAG = "XChangeStatisticsFragment";

    private static final int RC_FETCH_STATISTICS = 401;

    private TextView fragmentTitle;
    private ProgressBar progressBar;
    private Button btnSwitchToDollar;
    private Button btnSwitchToEuro;
    private TextView firstRateSubTitle;
    private TextView firstRateDateField;
    private TextView firstRateValueField;
    private TextView lastRateSubTitle;
    private TextView lastRateDateField;
    private TextView lastRateValueField;
    private TextView highestRateSubTitle;
    private TextView highestRateDateField;
    private TextView highestRateValueField;
    private TextView lowestRateSubTitle;
    private TextView lowestRateDateField;
    private TextView lowestRateValueField;
    private TextView averageRateSubTitle;
    private TextView averageRateValueField;
    private TextView changePercentSubTitle;
    private TextView changePercentField;
    private TextView numRecordsSubTitle;
    private TextView numRecordsField;

    private XChangeStatisticsDbService xChangeStatisticsDbService;
    private DateFormat dateFormat;

    private static boolean showDollar = true;

    private int titleResourceId;
    private Duration statisticsDuration;

    public XChangeStatisticsFragment(int tabSource) {
        switch (tabSource) {
            case TAB_WEEK:
                titleResourceId = R.string.title_statistics_week;
                statisticsDuration = Duration.ofDays(7);
                break;
            case TAB_MONTH:
                titleResourceId = R.string.title_statistics_month;
                statisticsDuration = Duration.ofDays(30);
                break;
            case TAB_ALL:
                titleResourceId = R.string.title_statistics_overall;
                statisticsDuration = Duration.ofDays(100 * 365);
                break;
            default:
                titleResourceId = R.string.title_statistics_day;
                statisticsDuration = Duration.ofDays(1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View fragmentView = layoutInflater.inflate(R.layout.fragment_xchangestatistics, viewGroup);
        initServices();
        bindComponents(fragmentView);
        return fragmentView;
    }

    @Override
    public void onResume() {
        setTitle();
        toggleProgressBar(true);
        fetchData();
        super.onResume();
    }

    private void initServices() {
        xChangeStatisticsDbService = ((MyApplication) getActivity().getApplication()).getXChangeStatisticsDbService();
        dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
    }

    private void bindComponents(View fragmentView) {
        fragmentTitle = fragmentView.findViewById(R.id.title_xchange_statistics);
        progressBar = fragmentView.findViewById(R.id.progressBarInfo);
        btnSwitchToDollar = fragmentView.findViewById(R.id.btn_fiat_dollar);
        btnSwitchToEuro = fragmentView.findViewById(R.id.btn_fiat_euro);
        firstRateSubTitle = fragmentView.findViewById(R.id.subtitle_first_rate);
        firstRateDateField = fragmentView.findViewById(R.id.out_date_first);
        firstRateValueField = fragmentView.findViewById(R.id.out_rate_first);
        lastRateSubTitle = fragmentView.findViewById(R.id.subtitle_last_rate);
        lastRateDateField = fragmentView.findViewById(R.id.out_date_last);
        lastRateValueField = fragmentView.findViewById(R.id.out_rate_last);
        highestRateSubTitle = fragmentView.findViewById(R.id.subtitle_highest_rate);
        highestRateDateField = fragmentView.findViewById(R.id.out_date_highest);
        highestRateValueField = fragmentView.findViewById(R.id.out_rate_highest);
        lowestRateSubTitle = fragmentView.findViewById(R.id.subtitle_lowest_rate);
        lowestRateDateField = fragmentView.findViewById(R.id.out_date_lowest);
        lowestRateValueField = fragmentView.findViewById(R.id.out_rate_lowest);
        averageRateSubTitle = fragmentView.findViewById(R.id.subtitle_average_rate);
        averageRateValueField = fragmentView.findViewById(R.id.out_rate_average);
        changePercentSubTitle = fragmentView.findViewById(R.id.subtitle_change);
        changePercentField = fragmentView.findViewById(R.id.out_change);
        numRecordsSubTitle = fragmentView.findViewById(R.id.subtitle_num_records);
        numRecordsField = fragmentView.findViewById(R.id.out_num_records);
        btnSwitchToDollar.setOnClickListener(v -> {
            showDollar = true;
            toggleProgressBar(false);
            fetchData();
        });
        btnSwitchToEuro.setOnClickListener(v -> {
            showDollar = false;
            toggleProgressBar(true);
            fetchData();
        });
    }

    private void setTitle() {
        fragmentTitle.setText(titleResourceId);
    }

    private void toggleProgressBar(boolean show) {
        int progressBarVisibility = show ? View.VISIBLE : View.GONE;
        int componentVisibility = show ? View.GONE : View.VISIBLE;
        progressBar.setVisibility(progressBarVisibility);
        btnSwitchToDollar.setVisibility(componentVisibility);
        btnSwitchToEuro.setVisibility(componentVisibility);
        firstRateSubTitle.setVisibility(componentVisibility);
        firstRateDateField.setVisibility(componentVisibility);
        firstRateValueField.setVisibility(componentVisibility);
        lastRateSubTitle.setVisibility(componentVisibility);
        lastRateDateField.setVisibility(componentVisibility);
        lastRateValueField.setVisibility(componentVisibility);
        highestRateSubTitle.setVisibility(componentVisibility);
        highestRateDateField.setVisibility(componentVisibility);
        highestRateValueField.setVisibility(componentVisibility);
        lowestRateSubTitle.setVisibility(componentVisibility);
        lowestRateDateField.setVisibility(componentVisibility);
        lowestRateValueField.setVisibility(componentVisibility);
        averageRateSubTitle.setVisibility(componentVisibility);
        averageRateValueField.setVisibility(componentVisibility);
        changePercentSubTitle.setVisibility(componentVisibility);
        changePercentField.setVisibility(componentVisibility);
        numRecordsSubTitle.setVisibility(componentVisibility);
        numRecordsField.setVisibility(componentVisibility);
    }

    private void fetchData() {
        Log.d(TAG, String.format("Fetch data for duration %s", statisticsDuration));
        btnSwitchToDollar.setEnabled(false);
        btnSwitchToEuro.setEnabled(false);
        firstRateDateField.setText("");
        firstRateValueField.setText("");
        lastRateDateField.setText("");
        lastRateValueField.setText("");
        highestRateDateField.setText("");
        highestRateValueField.setText("");
        lowestRateDateField.setText("");
        lowestRateValueField.setText("");
        averageRateValueField.setText("");
        changePercentField.setText("");
        numRecordsField.setText("");
        xChangeStatisticsDbService.gatherStatisticsForDuration(this, RC_FETCH_STATISTICS, statisticsDuration);
    }

    @Override
    public void onServiceSuccess(int requestCode) {
        Log.e(TAG, String.format("Unexpected success without data for requestCode[%d]", requestCode));
        toggleProgressBar(false);
    }

    @Override
    public void onServiceError(int requestCode, int errorCode, String errorMessage) {
        Log.e(TAG, String.format("Error when calling requestCode[%d]: error[%d] %s", requestCode, errorCode, errorMessage));
        toggleProgressBar(false);
    }

    @Override
    public void onServiceSuccess(int requestCode, XChangeStatistics xChangeStatistics) {
        Log.d(TAG, "Statistics received");
        btnSwitchToDollar.setEnabled(!showDollar);
        btnSwitchToEuro.setEnabled(showDollar);
        numRecordsField.setText(Integer.toString(xChangeStatistics.getNumRates()));

        if (xChangeStatistics.getNumRates() > 0) {
            firstRateDateField.setText(formatDate(xChangeStatistics.getFirstRate()));
            firstRateValueField.setText(formatFiat(xChangeStatistics.getFirstRate()));
            lastRateDateField.setText(formatDate(xChangeStatistics.getLastRate()));
            lastRateValueField.setText(formatFiat(xChangeStatistics.getLastRate()));
            averageRateValueField.setText(formatFiat(xChangeStatistics.getAverageRate()));
            final double change = XChangeRateChangeCalculator.changeInPercent(xChangeStatistics.getFirstRate(), xChangeStatistics.getLastRate());
            changePercentField.setText(String.format("%.2f %%", change));
            changePercentField.setTextColor(getResources().getColor(change >= 0d ? R.color.change_plus : R.color.change_minus, getContext().getTheme()));
            highestRateDateField.setText(formatDate(xChangeStatistics.getHighestRate()));
            highestRateValueField.setText(formatFiat(xChangeStatistics.getHighestRate()));
            lowestRateDateField.setText(formatDate(xChangeStatistics.getLowestRate()));
            lowestRateValueField.setText(formatFiat(xChangeStatistics.getLowestRate()));
        }
        toggleProgressBar(false);
    }

    private String formatDate(XChangeRate rate) {
        return dateFormat.format(rate.checkedAt);
    }

    private String formatFiat(XChangeRate rate) {
        return showDollar ? FiatFormatter.formatFiat("$", rate.btcDollarRate) : FiatFormatter.formatFiat("€", rate.btcEuroRate);
    }
}
