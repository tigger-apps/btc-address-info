package at.truetigger.android.btcaddressinfo.db.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.math.BigDecimal;
import java.util.Date;

import at.truetigger.android.btcaddressinfo.blockexplorer.BtcAddressInfo;
import at.truetigger.android.btcaddressinfo.db.converter.BigDecimalConverter;
import at.truetigger.android.btcaddressinfo.db.converter.TimestampConverter;

@Entity(tableName = "address_check")
@TypeConverters({TimestampConverter.class, BigDecimalConverter.class})
public class AddressCheck {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public Long id;

    @ColumnInfo(name = "btc_address")
    @NonNull
    public String btcAddress;

    @ColumnInfo(name = "btc_amount")
    @NonNull
    public BigDecimal btcAmount;

    @ColumnInfo(name = "num_transactions")
    @NonNull
    public Integer numTransactions;

    @ColumnInfo(name = "num_funded_txo")
    @NonNull
    public Integer numFundedTxo;

    @ColumnInfo(name = "num_spent_txo")
    @NonNull
    public Integer numSpentTxo;

    @ColumnInfo(name = "raw_result")
    @NonNull
    public String rawResult;

    @ColumnInfo(name = "checked_at")
    @NonNull
    public Date checkedAt;

    @Override
    public String toString() {
        return "AddressCheck[address=" + btcAddress + ",amount=" + btcAmount.toPlainString() + "]";
    }

    public static AddressCheck forBtcAddressInfo(BtcAddressInfo btcAddressInfo) {
        AddressCheck addressCheck = new AddressCheck();
        addressCheck.btcAddress = btcAddressInfo.getAddress();
        addressCheck.btcAmount = btcAddressInfo.getSumAmount();
        addressCheck.numTransactions = btcAddressInfo.getNumTransactions();
        addressCheck.numFundedTxo = btcAddressInfo.getNumFundedTXO();
        addressCheck.numSpentTxo = btcAddressInfo.getNumSpentTXO();
        addressCheck.rawResult = btcAddressInfo.getRawResult();
        addressCheck.checkedAt = new Date();
        return addressCheck;
    }
}

