package at.truetigger.android.btcaddressinfo.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import at.truetigger.android.btcaddressinfo.db.entity.Address;

@Dao
public interface AddressDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insert(Address address);

    @Query("SELECT * FROM address WHERE btc_address = :btcAddress")
    Address findByBtcAddress(String btcAddress);

    @Query("SELECT * FROM address ORDER BY created_at ASC")
    List<Address> findAll();

    @Update
    void update(Address address);

    @Delete
    void delete(Address address);
}
