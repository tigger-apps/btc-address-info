package at.truetigger.android.btcaddressinfo.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.AddressInfo;
import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.db.AddressDbService;
import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;

public class ShowQRCodeActivity extends AppCompatActivity implements
        AddressDbService.AddressDbServiceCallback {

    private static final int RC_DB_READ_ADDRESS = 301;

    private static final int QRCODE_SIZE_PERCENT_OF_SCREEN = 75;
    private static final int QRCODE_MARGIN = 2;  // in QR code "pixel"

    private static final String TAG = "ShowQRCodeActivity";

    private AddressDbService addressDbService;
    private ImageView qrImageView;
    private ProgressBar progressBar;
    private TextView customName;
    private TextView addressOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        initServices();
        bindComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String address = getAddressFromExtras();
        if (address == null) {
            finish();
        } else {
            toggleProgressBar(true);
            addressDbService.getAddressDetails(address, this, RC_DB_READ_ADDRESS);
        }
    }

    @Override
    public void onServiceSuccess(int requestCode) {
        Log.e(TAG, "Unexpected service success for RC " + requestCode);
        finish(); // back to previous activity
    }

    @Override
    public void onServiceError(int requestCode, int errorCode, String errorMessage) {
        Log.e(TAG, "Generic error for RC " + requestCode + ": errorCode=" + errorCode + ", errorMsg=" + errorMessage);
        finish(); // back to previous activity
    }

    @Override
    public void onServiceSuccess(int requestCode, Address address) {
    }

    @Override
    public void onServiceSuccess(int requestCode, Address address, AddressCheck addressCheck) {
        customName.setText(address.customName);
        addressOutput.setText(address.btcAddress);
        Bitmap qrCode = generateQRCode(address.btcAddress);
        qrImageView.setImageBitmap(qrCode);
        toggleProgressBar(false);
    }

    @Override
    public void onServiceSuccess(int requestCode, List<AddressInfo> addressList) {
    }

    private void initServices() {
        addressDbService = ((MyApplication) getApplication()).getAddressDbService();
    }

    private void bindComponents() {
        customName = findViewById(R.id.out_customName);
        addressOutput = findViewById(R.id.out_address);
        progressBar = findViewById(R.id.progressBarInfo);
        qrImageView = findViewById(R.id.img_qrcode);
    }

    private Bitmap generateQRCode(String btcAddress) {

        int qrCodeSizeInPx = calculatePreferredQrCodeSize();
        int qrCodeDark  = getResources().getColor(R.color.dark, null);
        int qrCodeLight = getResources().getColor(R.color.light, null);

        Map<EncodeHintType, Object> encodeHints = new EnumMap<>(EncodeHintType.class);
        encodeHints.put(EncodeHintType.MARGIN, QRCODE_MARGIN);

        try {
            BitMatrix result = new MultiFormatWriter().encode(btcAddress, BarcodeFormat.QR_CODE, qrCodeSizeInPx, qrCodeSizeInPx, encodeHints);
            int w = result.getWidth();
            int h = result.getHeight();
            int[] pixels = new int[w * h];
            for (int y = 0; y < h; y++) {
                int offset = y * w;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = result.get(x, y) ? qrCodeDark : qrCodeLight;
                }
            }
            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, qrCodeSizeInPx, 0, 0, w, h);
            return bitmap;
        } catch (Exception iae) {
            Log.e(TAG, "Error generate QR code: " + iae.getClass() + " (" + iae.getMessage());
            return null;
        }
    }

    @SuppressWarnings("DEPRECATION")
    private int calculatePreferredQrCodeSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        return (int)(Math.min(width, height) * QRCODE_SIZE_PERCENT_OF_SCREEN / 100);
    }

    private void toggleProgressBar(boolean showProgressBar) {
        if (showProgressBar) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private String getAddressFromExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(AddressInfoActivity.EXTRA_ADDRESS)) {
            return extras.getString(AddressInfoActivity.EXTRA_ADDRESS);
        }
        return null;
    }
}
