package at.truetigger.android.btcaddressinfo.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;

@Dao
public interface AddressCheckDao {

    @Insert
    Long insert(AddressCheck addressCheck);

    @Query("SELECT * FROM address_check WHERE id = :id")
    AddressCheck findById(Long id);

    @Query("SELECT * FROM address_check ORDER BY checked_at ASC")
    List<AddressCheck> findAll();
}
