package at.truetigger.android.btcaddressinfo.util;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.activity.XChangeStatisticsFragment;

public class XChangeStatisticsFragmentAdapter extends FragmentStateAdapter {

    public final static int TAB_DAY = 1;
    public final static int TAB_WEEK = 2;
    public final static int TAB_MONTH = 3;
    public static final int TAB_ALL = 4;

    public static final int[] TAB_NAMES = { R.string.tab_day, R.string.tab_week, R.string.tab_month, R.string.tab_overall };

    public XChangeStatisticsFragmentAdapter(FragmentManager fragmentManager, Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment;
        switch(position) {
            case 1: return new XChangeStatisticsFragment(TAB_WEEK);
            case 2: return new XChangeStatisticsFragment(TAB_MONTH);
            case 3: return new XChangeStatisticsFragment(TAB_ALL);
            default: return new XChangeStatisticsFragment(TAB_DAY);
        }
    }

    @Override
    public int getItemCount() {
        return TAB_NAMES.length;
    }
}
