package at.truetigger.android.btcaddressinfo.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.math.BigDecimal;
import java.util.Date;

import at.truetigger.android.btcaddressinfo.db.converter.BigDecimalConverter;
import at.truetigger.android.btcaddressinfo.db.converter.TimestampConverter;

@Entity(tableName = "xchange_rate")
@TypeConverters({BigDecimalConverter.class, TimestampConverter.class})
public class XChangeRate {

    @PrimaryKey(autoGenerate = true)
    public Long id;

    @ColumnInfo(name = "btc_euro_rate")
    public BigDecimal btcEuroRate;

    @ColumnInfo(name = "btc_dollar_rate")
    public BigDecimal btcDollarRate;

    @ColumnInfo(name = "checked_at")
    public Date checkedAt;

    @Override
    public String toString() {
        return "XChangeRate[id=" + id + ", euroRate=" + btcEuroRate + ", dollarRate=" + btcDollarRate + "]";
    }

    public static XChangeRate forEuroAndDollar(BigDecimal btcEuroRate, BigDecimal btcDollarRate) {
        return forEuroDollarAndTimestamp(btcEuroRate, btcDollarRate, new Date());
    }

    public static XChangeRate forEuroDollarAndTimestamp(BigDecimal btcEuroRate, BigDecimal btcDollarRate, Date checkedAt) {
        XChangeRate xChangeRate = new XChangeRate();
        xChangeRate.btcEuroRate = btcEuroRate;
        xChangeRate.btcDollarRate = btcDollarRate;
        xChangeRate.checkedAt = checkedAt;
        return xChangeRate;
    }
}
