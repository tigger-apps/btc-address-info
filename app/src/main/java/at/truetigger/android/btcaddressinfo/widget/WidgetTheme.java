package at.truetigger.android.btcaddressinfo.widget;

public enum WidgetTheme {
    DARK,
    LIGHT,
}
