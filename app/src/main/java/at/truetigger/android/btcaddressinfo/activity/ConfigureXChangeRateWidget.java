package at.truetigger.android.btcaddressinfo.activity;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

import at.truetigger.android.btcaddressinfo.MyApplication;
import at.truetigger.android.btcaddressinfo.R;
import at.truetigger.android.btcaddressinfo.widget.WidgetCurrency;
import at.truetigger.android.btcaddressinfo.widget.WidgetTheme;
import at.truetigger.android.btcaddressinfo.widget.XChangeRateWidget;
import at.truetigger.android.btcaddressinfo.widget.XChangeRateWidgetConfig;

public class ConfigureXChangeRateWidget  extends AppCompatActivity {

    private static final String TAG = "ConfigureXChangeRateWidget";

    public static final String RATE_WIDGET_CURRENCY_SETTING_KEY = "RateWidgetCurrency";
    public static final String RATE_WIDGET_THEME_SETTING_KEY = "RateWidgetTheme";
    public static final String RECONFIGURE_WIDGET = "reconfigureWidget";

    private int widgetId;
    private boolean reconfigureActionFlag;

    private RadioButton radioButtonCurrencyDollar;
    private RadioButton radioButtonCurrencyEuro;
    private RadioButton radioButtonThemeLight;
    private RadioButton radioButtonThemeDark;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_xchangerate_widget);
        setResult(RESULT_CANCELED);
        extractWidgetId();
        detectReconfigureAction();
        XChangeRateWidgetConfig currentConfig = readWidgetConfig(widgetId);
        initComponents(currentConfig);
    }

    private void extractWidgetId() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        } else {
            widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        }
        Log.d(TAG, "Extracted widgetId: " + widgetId);
    }

    private void detectReconfigureAction() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            reconfigureActionFlag = extras.getBoolean(RECONFIGURE_WIDGET, false);
        } else {
            reconfigureActionFlag = false;
        }
        Log.d(TAG, "ReconfigureAction: " + reconfigureActionFlag);
    }

    private XChangeRateWidgetConfig readWidgetConfig(int appWidgetId) {
        SharedPreferences sharedPreferences = getSharedPreferences(MyApplication.PREFERENCES_NAME, Context.MODE_PRIVATE);

        final WidgetCurrency currency = WidgetCurrency.valueOf(sharedPreferences.getString(RATE_WIDGET_CURRENCY_SETTING_KEY + appWidgetId, WidgetCurrency.USD.name()));
        final WidgetTheme theme = WidgetTheme.valueOf(sharedPreferences.getString(RATE_WIDGET_THEME_SETTING_KEY + appWidgetId, WidgetTheme.LIGHT.name()));
        return new XChangeRateWidgetConfig(currency, theme);
    }

    private void storeWidgetConfig() {
        if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            Log.d(TAG, "No valid widget ID, cannot store widget config");
            return;
        }

        storeSelectedConfig();

        Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE, null, this, XChangeRateWidget.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        sendBroadcast(intent);
        Log.d(TAG, "Fire update via configuration for widget[" + widgetId + "]");

        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        setResult(RESULT_OK, resultValue);

        if (reconfigureActionFlag) {
            Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
            mainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mainActivity.putExtra(MainActivity.CLOSE_IMMEDIATELY, true);
            startActivity(mainActivity);
        } else {
            finish();
        }
    }

    private void storeSelectedConfig() {
        final WidgetCurrency currency = radioButtonCurrencyDollar.isChecked() ? WidgetCurrency.USD : WidgetCurrency.EUR;
        final WidgetTheme theme = radioButtonThemeDark.isChecked() ? WidgetTheme.DARK : WidgetTheme.LIGHT;
        SharedPreferences sharedPreferences = getSharedPreferences(MyApplication.PREFERENCES_NAME, MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(RATE_WIDGET_CURRENCY_SETTING_KEY + widgetId, currency.name());
        sharedPreferencesEditor.putString(RATE_WIDGET_THEME_SETTING_KEY + widgetId, theme.name());
        sharedPreferencesEditor.commit();
        Log.d(TAG, "Store config for widgetId=" + widgetId + ": currency=" + currency.name() + ", theme=" + theme.name());
    }

    private void initComponents(XChangeRateWidgetConfig currentConfig) {
        radioButtonCurrencyDollar = findViewById(R.id.widget_option_dollar);
        radioButtonCurrencyDollar.setChecked(currentConfig.getCurrency() == WidgetCurrency.USD);
        radioButtonCurrencyEuro = findViewById(R.id.widget_option_euro);
        radioButtonCurrencyEuro.setChecked(currentConfig.getCurrency() == WidgetCurrency.EUR);
        radioButtonThemeLight = findViewById(R.id.widget_option_themeLight);
        radioButtonThemeLight.setChecked(currentConfig.getTheme() == WidgetTheme.LIGHT);
        radioButtonThemeDark = findViewById(R.id.widget_option_themeDark);
        radioButtonThemeDark.setChecked(currentConfig.getTheme() == WidgetTheme.DARK);

        saveButton = findViewById(R.id.widget_btn_save);
        saveButton.setOnClickListener(view -> storeWidgetConfig());
    }
}
