package at.truetigger.android.btcaddressinfo.util;

import android.content.Context;
import android.net.ConnectivityManager;

public class ConnectivityChecker {

    private ConnectivityManager connectivityManager;

    public ConnectivityChecker(Context ctx) {
        connectivityManager = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public boolean hasNetworkConnectivity() {
        return connectivityManager.getActiveNetwork() != null;
    }
}
