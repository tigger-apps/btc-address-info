package at.truetigger.android.btcaddressinfo;

public interface ServiceCallback {

    /**
     * Is called in case of success.
     *
     * @param requestCode The integer request code originally supplied at invocation of the service,
     *                    allowing you to identify who this result came from.
     */
    void onServiceSuccess(int requestCode);

    /**
     * Is called in case of error.
     *
     * @param requestCode  The integer request code originally supplied at invocation of the service,
     *                     allowing you to identify who this result came from.
     * @param errorCode    Indicate the type of error
     * @param errorMessage An error message with some helpful information about the error
     */
    void onServiceError(int requestCode, int errorCode, String errorMessage);
}
