package at.truetigger.android.btcaddressinfo.util;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

public class DurationToDateConverterTest {

    @Test
    public void convertUtcDate() throws Exception {
        Date baseDate = createDate("2021-11-20 13:14:15", "UTC");
        Duration duration = Duration.ofHours(20);

        Date convertedDate = DurationToDateConverter.dateMinusDuration(baseDate, duration);

        // expected: 20h before
        Date expectedDate = createDate("2021-11-19 17:14:15", "UTC");
        assertThat(convertedDate.getTime(), is(expectedDate.getTime()));
    }

    @Test
    public void convertViennaTime() throws Exception {
        Date baseDate = createDate("2021-10-31 09:08:07", "Europe/Vienna");
        Duration duration = Duration.ofHours(24);

        Date convertedDate = DurationToDateConverter.dateMinusDuration(baseDate, duration);

        // expected: 1 day before but this was CEST
        Date expectedDate = createDate("2021-10-30 10:08:07", "Europe/Vienna");
        assertThat(convertedDate.getTime(), is(expectedDate.getTime()));
    }

    private Date createDate(String datetime, String timezone) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of(timezone)));
        return sdf.parse(datetime);
    }
}
