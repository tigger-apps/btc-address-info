package at.truetigger.android.btcaddressinfo.io;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CsvDataExporterTest {

    private CsvDataExporter classUnderTest;

    @Before
    public void setUp() {
        classUnderTest = new CsvDataExporter();
    }

    @Test
    public void exportNullString() {
        String data = null;
        String asCsv = classUnderTest.formatString(data);
        assertThat(asCsv, is(""));
    }

    @Test
    public void exportEmptyString() {
        String data = "";
        String asCsv = classUnderTest.formatString(data);
        assertThat(asCsv, is("\"\""));
    }

    @Test
    public void exportSimpleString() {
        String data = "1999";
        String asCsv = classUnderTest.formatString(data);
        assertThat(asCsv, is("\"1999\""));
    }

    @Test
    public void exportStringWithQuoteCharacter() {
        String data = "\"Hello Harry\", Dumbledore says";
        String asCsv = classUnderTest.formatString(data);
        assertThat(asCsv, is("\"\"\"Hello Harry\"\", Dumbledore says\""));
    }

    @Test
    public void exportNullDate() {
        Date data = null;
        String asCsv = classUnderTest.formatDate(data);
        assertThat(asCsv, is(""));
    }

    @Test
    public void exportDate() {
        Date date = new Date(1619829752000L);  // Saturday, May 1, 2021 at 12:42:32 AM Greenwich Mean Time
        String asCsv  = classUnderTest.formatDate(date);
        assertThat(asCsv, is("2021-05-01 00:42:32"));
    }

    @Test
    public void exportNullLong() {
        Long data = null;
        String asCsv = classUnderTest.formatLong(data);
        assertThat(asCsv, is(""));
    }

    @Test
    public void exportZeroLong() {
        Long data = 0L;
        String asCsv = classUnderTest.formatLong(data);
        assertThat(asCsv, is("0"));
    }

    @Test
    public void exportHugeLong() {
        Long data = 987654321L;
        String asCsv = classUnderTest.formatLong(data);
        assertThat(asCsv, is("987654321"));
    }

    @Test
    public void exportNullBigDecimal() {
        BigDecimal data = null;
        String asCsv = classUnderTest.formatBigDecimal(data);
        assertThat(asCsv, is(""));
    }

    @Test
    public void exportSimpleBigDecimal() {
        BigDecimal data = BigDecimal.ZERO;
        String asCsv = classUnderTest.formatBigDecimal(data);
        assertThat(asCsv, is("\"0\""));
    }

    @Test
    public void exportComplexBigDecimal() {
        BigDecimal data = new BigDecimal("9313113.31378").divide(BigDecimal.TEN, 5, RoundingMode.HALF_UP);
        String asCsv = classUnderTest.formatBigDecimal(data);
        assertThat(asCsv, is("\"931311.33138\""));
    }

    @Test
    public void exportComplexNegativeBigDecimal() {
        BigDecimal data = new BigDecimal("-1312.001").setScale(8);
        String asCsv = classUnderTest.formatBigDecimal(data);
        assertThat(asCsv, is("\"-1312.00100000\""));
    }

    @Test
    public void testContentType() {
        assertThat(classUnderTest.getContentType(), is("text/csv"));
    }
}
