package at.truetigger.android.btcaddressinfo.util;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class BtcAddressExtractorTest {

    private static final String BASE58_ADDRESS = "387x87fbbtyeghGiUtvBYBDtHFgziy8VG9";
    private static final String SEGWIT_ADDRESS = "bc1q2z78uyg4ckcwrzp3pnvfjcg6w78yld6h8rf5z7";

    private BtcAddressExtractor classUnderTest;

    @Before
    public void setUp() {
        classUnderTest = new BtcAddressExtractor();
    }

    @Test
    public void itShouldReturnAPlainBase58Address() {
        String input = BASE58_ADDRESS;
        assertThat(classUnderTest.extractBtcAddress(input), is(BASE58_ADDRESS));
    }

    @Test
    public void itShouldReturnAPlainSegwitAddress() {
        String input = SEGWIT_ADDRESS;
        assertThat(classUnderTest.extractBtcAddress(input), is(SEGWIT_ADDRESS));
    }

    @Test
    public void itShouldStripBitcoinPrefix() {
        String input = "bitcoin:" + BASE58_ADDRESS;
        assertThat(classUnderTest.extractBtcAddress(input), is(BASE58_ADDRESS));
    }

    @Test
    public void itShouldStripBitcoinParams() {
        String input = SEGWIT_ADDRESS + "?label=test";
        assertThat(classUnderTest.extractBtcAddress(input), is(SEGWIT_ADDRESS));
    }

    @Test
    public void itShouldExtractAddressFromBip21Uri() {
        String input = "bitcoin:" + SEGWIT_ADDRESS + "?label=test";
        assertThat(classUnderTest.extractBtcAddress(input), is(SEGWIT_ADDRESS));
    }

    @Test
    public void itShouldRejectTooShortAddresses() {
        String input = "1b3";
        assertThat(classUnderTest.extractBtcAddress(input), is(nullValue()));
    }

    @Test
    public void itShouldRejectTooLongAddresses() {
        String input = "1aaaaaaaaaabbbbbbbbccccccccddddddddeeeeeeeeffffffffgggggggghhhhhhhhjjjjjjjjkkkkkkkkmmmmmmmmnnnnnnnn";
        assertThat(classUnderTest.extractBtcAddress(input), is(nullValue()));
    }

    @Test
    public void itShouldAcceptUppercaseBech32Addresses() {
        String input = SEGWIT_ADDRESS.toUpperCase();
        assertThat(classUnderTest.extractBtcAddress(input), is(SEGWIT_ADDRESS));
    }
}
