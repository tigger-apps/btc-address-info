package at.truetigger.android.btcaddressinfo.util;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class FiatFormatterTest {

    @Test
    public void formatCentFiatValue() {
        BigDecimal centFiatFormat = new BigDecimal("0.01");
        String formattedOutput = FiatFormatter.formatFiat("¤", centFiatFormat);
        assertThat(formattedOutput, is("¤ 0.01"));
    }

    @Test
    public void formatSmallFiatValue() {
        BigDecimal smallFiatFormat = new BigDecimal("65.4321");
        String formattedOutput = FiatFormatter.formatFiat("¤", smallFiatFormat);
        assertThat(formattedOutput, is("¤ 65.43"));
    }

    @Test
    public void formatMediumFiatValue() {
        BigDecimal mediumFiatFormat = new BigDecimal("1234.567");
        String formattedOutput = FiatFormatter.formatFiat("¤", mediumFiatFormat);
        assertThat(formattedOutput, is("¤ 1,234.57"));
    }

    @Test
    public void formatLargeFiatValue() {
        BigDecimal largeFiatValue = new BigDecimal("999999999.99");
        String formattedOutput = FiatFormatter.formatFiat("¤", largeFiatValue);
        assertThat(formattedOutput, is("¤ 999,999,999.99"));
    }

    @Test
    public void formatHugeFiatAmount() {
        BigDecimal largeFiatValue = new BigDecimal("45111222333.557");
        String formattedOutput = FiatFormatter.formatFiat("¤", largeFiatValue);
        assertThat(formattedOutput, is("¤ 45,111,222,333.56"));
    }
}
