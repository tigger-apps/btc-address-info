package at.truetigger.android.btcaddressinfo.util;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BtcFormatterTest {

    @Test
    public void formatHugeAmount() {
        BigDecimal hugeAmount = new BigDecimal("123.987654321");
        String formattedAmount = BtcFormatter.formatCrypto(hugeAmount);
        assertThat(formattedAmount, is("123.988 BTC"));
    }

    @Test
    public void formatSmallAmount() {
        BigDecimal smallAmount = new BigDecimal("0.0025");
        String formattedAmount = BtcFormatter.formatCrypto(smallAmount);
        assertThat(formattedAmount, is("2.5 mBTC"));
    }

    @Test
    public void formatAlignedAmount() {
        BigDecimal alignedAmount = BigDecimal.TEN;
        String formattedAmount = BtcFormatter.formatCrypto(alignedAmount);
        assertThat(formattedAmount, is("10 BTC"));
    }

    @Test
    public void formatTooSmallAmount() {
        BigDecimal tooSmallAmount = new BigDecimal("0.00000049");
        String formattedAmount = BtcFormatter.formatCrypto(tooSmallAmount);
        assertThat(formattedAmount, is("0 mBTC"));
    }
}
