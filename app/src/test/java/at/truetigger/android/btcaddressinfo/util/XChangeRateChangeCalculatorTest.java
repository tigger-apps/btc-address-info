package at.truetigger.android.btcaddressinfo.util;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import java.math.BigDecimal;

import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

public class XChangeRateChangeCalculatorTest {

    private static final BigDecimal ONE_TOUSAND = new BigDecimal("1000");
    private static final BigDecimal FIVE_TOUSAND = new BigDecimal("5000");
    private static final BigDecimal EURO_VALUE = new BigDecimal("1.23");
    
    @Test
    public void testNoChange() {
        XChangeRate oldRate = XChangeRate.forEuroAndDollar(EURO_VALUE, ONE_TOUSAND);
        XChangeRate newRate = XChangeRate.forEuroAndDollar(EURO_VALUE, ONE_TOUSAND);

        double change = XChangeRateChangeCalculator.changeInPercent(oldRate, newRate);

        assertThat(change, is(0d));
    }

    @Test
    public void testRisingRate() {
        XChangeRate oldRate = XChangeRate.forEuroAndDollar(EURO_VALUE, ONE_TOUSAND);
        XChangeRate newRate = XChangeRate.forEuroAndDollar(EURO_VALUE, FIVE_TOUSAND);

        double change = XChangeRateChangeCalculator.changeInPercent(oldRate, newRate);

        assertThat(change, is( 400d));
    }

    @Test
    public void testFallingRate() {
        XChangeRate oldRate = XChangeRate.forEuroAndDollar(EURO_VALUE, FIVE_TOUSAND);
        XChangeRate newRate = XChangeRate.forEuroAndDollar(EURO_VALUE, ONE_TOUSAND);

        double change = XChangeRateChangeCalculator.changeInPercent(oldRate, newRate);

        assertThat(change, is( -80d));
    }
}
