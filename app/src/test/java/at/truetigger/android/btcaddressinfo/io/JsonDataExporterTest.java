package at.truetigger.android.btcaddressinfo.io;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;
import at.truetigger.android.btcaddressinfo.db.dto.AllData;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class JsonDataExporterTest {

    private JsonDataExporter classUnderTest;

    private static final String BTC_ADDRESS = "15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18";
    private static final Date DATE = new Date(1619829752000L);  // Saturday, May 1, 2021 at 12:42:32 AM Greenwich Mean Time;

    @Before
    public void setUp() {
        classUnderTest = new JsonDataExporter();
    }

    @Test
    public void formatSpareAddress() throws JSONException {
        Address address = new Address();
        address.btcAddress = BTC_ADDRESS;
        address.createdAt = DATE;

        JSONObject jsonObject = classUnderTest.formatAddress(address);

        String expectedJson = "{\n"
            + "  \"btcAddress\": \"" + BTC_ADDRESS + "\",\n"
            + "  \"createdAt\":  \"2021-05-01T00:42:32+0000\"\n"
            + "}";
        JSONAssert.assertEquals(expectedJson, jsonObject.toString(), false);
    }

    @Test
    public void formatCompleteAddress() throws JSONException {
        Address address = new Address();
        address.btcAddress = BTC_ADDRESS;
        address.customName = "F-Droid Donate";
        address.addressType = "uTestType";
        address.addressCheckId = 1234L;
        address.createdAt = DATE;

        JSONObject jsonObject = classUnderTest.formatAddress(address);

        String expectedJson = "{\n"
                + "  \"btcAddress\":     \"" + BTC_ADDRESS + "\",\n"
                + "  \"customName\":     \"F-Droid Donate\",\n"
                + "  \"addressType\":    \"uTestType\",\n"
                + "  \"addressCheckId\": 1234,\n"
                + "  \"createdAt\":      \"2021-05-01T00:42:32+0000\"\n"
                + "}";
        JSONAssert.assertEquals(expectedJson, jsonObject.toString(), false);
    }

    @Test
    public void formatAddressCheck() throws JSONException {
        AddressCheck addressCheck = new AddressCheck();
        addressCheck.id = 987L;
        addressCheck.btcAddress = BTC_ADDRESS;
        addressCheck.btcAmount = new BigDecimal("12345678.3321");
        addressCheck.numTransactions = 12;
        addressCheck.numFundedTxo = 7;
        addressCheck.numSpentTxo = 2;
        addressCheck.checkedAt = DATE;

        JSONObject jsonObject = classUnderTest.formatAddressCheck(addressCheck);

        String expectedJson = "{"
                + "  \"id\":              987,\n"
                + "  \"btcAddress\":      \"" + BTC_ADDRESS + "\",\n"
                + "  \"btcAmount\":       \"12345678.3321\",\n"
                + "  \"numTransactions\": 12,\n"
                + "  \"numFundedTxo\":    7,\n"
                + "  \"numSpentTxo\":     2,\n"
                + "  \"checkedAt\":       \"2021-05-01T00:42:32+0000\"\n"
                + "}";
        JSONAssert.assertEquals(expectedJson, jsonObject.toString(), false);
    }

    @Test
    public void formatXchangeRate() throws JSONException {
        XChangeRate xChangeRate = new XChangeRate();
        xChangeRate.id = 123L;
        xChangeRate.btcEuroRate = new BigDecimal("7654.321");
        xChangeRate.btcDollarRate = BigDecimal.TEN;
        xChangeRate.checkedAt = DATE;

        JSONObject jsonObject = classUnderTest.formatXChangeRate(xChangeRate);

        String expectedJson = "{"
                + "  \"id\":            123,\n"
                + "  \"btcEuroRate\":   \"7654.321\",\n"
                + "  \"btcDollarRate\": \"10\",\n"
                + "  \"checkedAt\":     \"2021-05-01T00:42:32+0000\"\n"
                + "}";
        JSONAssert.assertEquals(expectedJson, jsonObject.toString(), false);
    }

    @Test
    public void formatMetaData() throws JSONException {
        Map<String, Object> metaData = new HashMap<>();
        metaData.put("abc", 123);
        metaData.put("date", DATE);
        metaData.put("name", "tigger");

        JSONObject jsonObject = classUnderTest.formatMetadata(metaData);

        String expectedJson  = "{"
                + "  \"abc\": \"123\","
                + "  \"date\": \"2021-05-01T00:42:32+0000\","
                + "  \"name\": \"tigger\""
                + "}";
        JSONAssert.assertEquals(expectedJson, jsonObject.toString(), false);
    }

    @Test
    public void formatRootObject() throws JSONException {
        AddressCheck addressCheck1 = new AddressCheck();
        addressCheck1.id = 20L;
        AddressCheck addressCheck2 = new AddressCheck();
        addressCheck2.id = 23L;
        addressCheck2.btcAddress = BTC_ADDRESS;
        XChangeRate xChangeRate = new XChangeRate();
        xChangeRate.id = 19L;
        List<Address> addressList = new ArrayList<>();
        List<AddressCheck> addressCheckList = new ArrayList<>();
        addressCheckList.add(addressCheck1);
        addressCheckList.add(addressCheck2);
        List<XChangeRate> xChangeRateList = new ArrayList<>();
        xChangeRateList.add(xChangeRate);
        AllData allData = new AllData(addressList, addressCheckList, xChangeRateList);

        JSONObject jsonObject = classUnderTest.buildJson(allData);

        String expectedJson = "{"
                + "  \"addresses\": [],\n"
                + "  \"addressChecks\": [{\"id\": 20}, {\"id\": 23, \"btcAddress\": \"" + BTC_ADDRESS + "\"}],\n"
                + "  \"xChangeRates\":  [{\"id\": 19}]\n"
                + "}";
        JSONAssert.assertEquals(expectedJson, jsonObject.toString(), false);
    }

    @Test
    public void readFromBackupJson() throws JSONException {
        String backupJson = "{\"metadata\":{\"appVersion\":\"0.0.5\",\"androidVersion\":\"30\",\"backupDate\":\"2021-05-09T18:48:10+0000\"},\"addresses\":[{\"btcAddress\":\"15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18\",\"customName\":\"F-Droid donations\",\"addressCheckId\":1,\"createdAt\":\"2021-05-09T17:37:54+0000\"}],\"addressChecks\":[{\"id\":1,\"btcAddress\":\"15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18\",\"btcAmount\":\"0.46790884\",\"numTransactions\":927,\"numFundedTxo\":878,\"numSpentTxo\":557,\"checkedAt\":\"2021-05-09T17:37:54+0000\"}],\"xChangeRates\":[{\"id\":1,\"btcEuroRate\":\"47287.1603\",\"btcDollarRate\":\"57516.5332\",\"checkedAt\":\"2021-05-09T17:25:39+0000\"},{\"id\":2,\"btcEuroRate\":\"47291.9163\",\"btcDollarRate\":\"57522.3181\",\"checkedAt\":\"2021-05-09T17:30:58+0000\"},{\"id\":3,\"btcEuroRate\":\"47257.7665\",\"btcDollarRate\":\"57480.7808\",\"checkedAt\":\"2021-05-09T17:37:53+0000\"},{\"id\":4,\"btcEuroRate\":\"47279.5840\",\"btcDollarRate\":\"57507.3181\",\"checkedAt\":\"2021-05-09T18:45:09+0000\"}]}";
        AllData allData = classUnderTest.fromJson(backupJson);
        assertThat(allData.getAddressList().get(0).btcAddress, is("15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18"));
        assertThat(allData.getAddressCheckList().size(), is(1));
        assertThat(allData.getXChangeRateList().size(), is(4));
    }

    @Test
    public void testContentType() {
        assertThat(classUnderTest.getContentType(), is("application/json"));
    }
}
