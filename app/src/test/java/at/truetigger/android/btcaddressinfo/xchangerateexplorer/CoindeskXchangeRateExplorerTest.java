package at.truetigger.android.btcaddressinfo.xchangerateexplorer;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CoindeskXchangeRateExplorerTest {

    private CoindeskXchangeRateExplorer classUnderTest;

    @Before
    public void setUp() {
        classUnderTest = new CoindeskXchangeRateExplorer();
    }

    @Test
    public void testDetectXchangeRates() throws JSONException {
        String input = "{\n"
        + "  \"time\": {\n"
        + "    \"updated\":    \"Apr 11, 2021 14:11:00 UTC\",\n"
        + "    \"updatedISO\": \"2021-04-11T14:11:00+00:00\",\n"
        + "    \"updateduk\":  \"Apr 11, 2021 at 15:11 BST\"\n"
        + "  },\n"
        + "  \"disclaimer\":   \"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org\",\n"
        + "  \"chartName\": \"Bitcoin\",\n"
        + "  \"bpi\": {\n"
        + "    \"USD\": {\n"
        + "      \"code\":        \"USD\",\n"
        + "      \"symbol\":      \"&#36;\",\n"
        + "      \"rate\":        \"59,417.9250\",\n"
        + "      \"description\": \"United States Dollar\",\n"
        + "      \"rate_float\":   59417.925\n"
        + "    },\n"
        + "    \"GBP\":{\n"
        + "      \"code\":        \"GBP\",\n"
        + "      \"symbol\":      \"&pound;\",\n"
        + "      \"rate\":        \"43,343.8314\",\n"
        + "      \"description\": \"British Pound Sterling\",\n"
        + "      \"rate_float\":  43343.8314\n"
        + "    },\n"
        + "    \"EUR\":{\n"
        + "      \"code\":        \"EUR\",\n"
        + "      \"symbol\":      \"&euro;\",\n"
        + "      \"rate\":        \"49,927.6940\",\n"
        + "      \"description\": \"Euro\",\n"
        + "      \"rate_float\":   49927.694\n"
        + "    }\n"
        + "  }\n"
        + "}";

        JSONObject coindeskRateResult = new JSONObject(input);

        Map<String, BigDecimal> rates = classUnderTest.toPriceInfo(coindeskRateResult);
        assertThat(rates.containsKey("USD"), is(true));
        assertThat(rates.get("USD").stripTrailingZeros(), is(new BigDecimal("59417.925")));
        assertThat(rates.containsKey("EUR"), is(true));
        assertThat(rates.get("EUR").stripTrailingZeros(), is(new BigDecimal("49927.694")));
    }
}
