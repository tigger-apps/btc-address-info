package at.truetigger.android.btcaddressinfo.blockexplorer;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BlockstreamBlockExplorerTest {

    private BlockstreamBlockExplorer classUnderTest;

    @Before
    public void setUp() {
        classUnderTest = new BlockstreamBlockExplorer();
    }

    @Test
    public void testTxInBlockchain() throws JSONException {
        String input = "{" +
                "\"address\":\"15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18\"," +
                "\"chain_stats\":{" +
                  "\"funded_txo_count\":873," +
                  "\"funded_txo_sum\":5214870621," +
                  "\"spent_txo_count\":557," +
                  "\"spent_txo_sum\":5168564809," +
                  "\"tx_count\":922}," +
                "\"mempool_stats\":{" +
                  "\"funded_txo_count\":0," +
                  "\"funded_txo_sum\":0," +
                  "\"spent_txo_count\":0," +
                  "\"spent_txo_sum\":0," +
                  "\"tx_count\":0}" +
                "}";
        JSONObject blockstreamResult = new JSONObject(input);

        BtcAddressInfo btcAddressInfo = classUnderTest.toBtcAddressInfo(blockstreamResult);

        assertThat(btcAddressInfo.getAddress(), is("15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18"));
        assertThat(btcAddressInfo.getNumTransactions(), is(922));
        assertThat(btcAddressInfo.getNumFundedTXO(), is(873));
        assertThat(btcAddressInfo.getNumSpentTXO(), is(557));
        assertThat(btcAddressInfo.getSumAmount(), is(new BigDecimal("0.46305812")));
    }

    @Test
    public void testTxInMempool() throws JSONException {
        String input = "{" +
                "\"address\":\"3AeWfLG7Hi4RXGtmK579Z7hEXbymAXPYwC\"," +
                "\"chain_stats\":{" +
                  "\"funded_txo_count\":8," +
                  "\"funded_txo_sum\":1150123560," +
                  "\"spent_txo_count\":1," +
                  "\"spent_txo_sum\":600000000," +
                  "\"tx_count\":9}," +
                "\"mempool_stats\":{" +
                  "\"funded_txo_count\":2," +
                  "\"funded_txo_sum\":49385941," +
                  "\"spent_txo_count\":0," +
                  "\"spent_txo_sum\":0," +
                  "\"tx_count\":2}" +
                "}";
        JSONObject blockstreamResult = new JSONObject(input);

        BtcAddressInfo btcAddressInfo = classUnderTest.toBtcAddressInfo(blockstreamResult);

        assertThat(btcAddressInfo.getAddress(), is("3AeWfLG7Hi4RXGtmK579Z7hEXbymAXPYwC"));
        assertThat(btcAddressInfo.getNumTransactions(), is(11));
        assertThat(btcAddressInfo.getNumFundedTXO(), is(10));
        assertThat(btcAddressInfo.getNumSpentTXO(), is(1));
        assertThat(btcAddressInfo.getSumAmount(), is(new BigDecimal("5.99509501")));
    }
}
