package at.truetigger.android.btcaddressinfo.db;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import at.truetigger.android.btcaddressinfo.blockexplorer.BtcAddressInfo;
import at.truetigger.android.btcaddressinfo.db.dao.AddressCheckDao;
import at.truetigger.android.btcaddressinfo.db.dao.AddressDao;
import at.truetigger.android.btcaddressinfo.db.entity.Address;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class AddressDaoTest {

    private AddressDao classUnderTest;
    private AddressCheckDao addressCheckDao;
    private AppDatabase db;

    private static final String TEST_ADDRESS_ARCHIVE = "1Archive1n2C579dMsAu3iC6tWzuQJz8dN";
    private static final String TEST_ADDRESS_FDROID = "15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18";

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        addressCheckDao = db.getAddressCheckDao();
        classUnderTest = db.getAddressDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void testFindAllAddresses() {
        // without addresses
        List<Address> emptyAddressList = classUnderTest.findAll();
        assertThat(emptyAddressList, hasSize(0));
        // with one address
        classUnderTest.insert(buildAddressRecordForAddress(TEST_ADDRESS_ARCHIVE));
        List<Address> addressListWithSingleAddress = classUnderTest.findAll();
        assertThat(addressListWithSingleAddress, hasSize(1));
        // with two addresses
        classUnderTest.insert(buildAddressRecordForAddress(TEST_ADDRESS_FDROID));
        List<Address> addressListWithTwoAddresses = classUnderTest.findAll();
        assertThat(addressListWithTwoAddresses, hasSize(2));
    }

    @Test
    public void testCannotSaveSameAddressTwice() {
        classUnderTest.insert(buildAddressRecordForAddress(TEST_ADDRESS_ARCHIVE));
        try {
            classUnderTest.insert(buildAddressRecordForAddress(TEST_ADDRESS_ARCHIVE));
            fail("Should not possible to add an address twice");
        } catch (SQLiteConstraintException sqlException) {
            assertThat(sqlException.getMessage().isEmpty(), is(false));
        }
    }

    @Test
    public void testUpdateAddress() {
        String customName = "instrumentedTestName";

        classUnderTest.insert(buildAddressRecordForAddress(TEST_ADDRESS_ARCHIVE));
        Address reloadedAddress = classUnderTest.findByBtcAddress(TEST_ADDRESS_ARCHIVE);
        assertThat(reloadedAddress.customName, is(nullValue()));

        reloadedAddress.customName = customName;
        classUnderTest.update(reloadedAddress);
        Address newReloadedAddress = classUnderTest.findByBtcAddress(TEST_ADDRESS_ARCHIVE);
        assertThat(newReloadedAddress.customName, is(customName));
    }

    @Test
    public void testFindUnknownAddress() {
        Address unknownAddress = classUnderTest.findByBtcAddress("Not stored");
        assertThat(unknownAddress, is(nullValue()));
    }

    private Address buildAddressRecordForAddress(String btcAddress) {
        AddressCheck addressCheck = AddressCheck.forBtcAddressInfo(BtcAddressInfo.from(btcAddress, 1, 2, 3, BigDecimal.ZERO, "dummyResult"));
        Long addressCheckId = addressCheckDao.insert(addressCheck);
        Address address = Address.forBtcAddress(btcAddress);
        address.addressCheckId = addressCheckId;
        return address;
    }
}
