package at.truetigger.android.btcaddressinfo.db;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import at.truetigger.android.btcaddressinfo.db.dao.AddressCheckDao;
import at.truetigger.android.btcaddressinfo.db.entity.AddressCheck;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class AddressCheckDaoTest {

    private AddressCheckDao classUnderTest;
    private AppDatabase db;

    private static final String TEST_ADDRESSES = "1Archive1n2C579dMsAu3iC6tWzuQJz8dN";
    private static final String RAW_RESULT = "simulatedEntry";

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        classUnderTest = db.getAddressCheckDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void testInsertAndReload() {
        BigDecimal sumAmount = new BigDecimal("0.025");
        int numTransactions = 4;
        int numFundedTXO = 3;
        int numSpentTXO = 2;

        Long insertId = classUnderTest.insert(buildAddressCheck(TEST_ADDRESSES, sumAmount, numTransactions, numFundedTXO, numSpentTXO));
        assertThat(insertId, is(not(nullValue())));

        AddressCheck reloadedAddressCheck = classUnderTest.findById(insertId);
        assertThat(reloadedAddressCheck.btcAddress, is(TEST_ADDRESSES));
        assertThat(reloadedAddressCheck.numTransactions, is(numTransactions));
        assertThat(reloadedAddressCheck.numFundedTxo, is(numFundedTXO));
        assertThat(reloadedAddressCheck.numSpentTxo, is(numSpentTXO));
        assertThat(reloadedAddressCheck.btcAmount, is(sumAmount));
        assertThat(reloadedAddressCheck.rawResult, is(RAW_RESULT));
    }

    @Test
    public void testFindAll() throws Exception {
        List<AddressCheck> addressCheckListBefore = classUnderTest.findAll();
        assertThat(addressCheckListBefore, hasSize(0));

        classUnderTest.insert(buildAddressCheck(TEST_ADDRESSES, BigDecimal.ONE, 1, 1, 0));
        Thread.sleep(10); // ensure second record has a later date
        classUnderTest.insert(buildAddressCheck(TEST_ADDRESSES, BigDecimal.TEN, 12, 2, 6));

        List<AddressCheck> addressCheckListAfter = classUnderTest.findAll();
        assertThat(addressCheckListAfter, hasSize(2));
        assertThat(addressCheckListAfter.get(0).btcAmount, is(BigDecimal.ONE));
        assertThat(addressCheckListAfter.get(1).btcAmount, is(BigDecimal.TEN));
    }

    private AddressCheck buildAddressCheck(String address, BigDecimal amount, int numTransactions,
                                           int numFundedTxo, int numSpentTxo) {
        AddressCheck addressCheck = new AddressCheck();
        addressCheck.btcAddress = address;
        addressCheck.btcAmount = amount;
        addressCheck.numTransactions = numTransactions;
        addressCheck.numFundedTxo = numFundedTxo;
        addressCheck.numSpentTxo = numSpentTxo;
        addressCheck.rawResult = RAW_RESULT;
        addressCheck.checkedAt = new Date();
        return addressCheck;
    }
}
