package at.truetigger.android.btcaddressinfo.db;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import at.truetigger.android.btcaddressinfo.db.dao.XChangeRateDao;
import at.truetigger.android.btcaddressinfo.db.entity.XChangeRate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

@RunWith(AndroidJUnit4.class)
public class XchangeRateDaoTest {

    private XChangeRateDao classUnderTest;
    private AppDatabase db;

    private static final BigDecimal EURO_RATE     = new BigDecimal("100000.0001");
    private static final BigDecimal DOLLAR_RATE   = new BigDecimal("111111.9999");
    private static final BigDecimal EURO_RATE_2   = new BigDecimal("10");
    private static final BigDecimal DOLLAR_RATE_2 = new BigDecimal("12");
    private static final BigDecimal EURO_RATE_3   = new BigDecimal("99999.0001");
    private static final BigDecimal DOLLAR_RATE_3 = new BigDecimal("9");

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        classUnderTest = db.getXChangeRateDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void testInsertAndReadXchangeRate() throws Exception {
        // store first record
        Date now = new Date();
        Long firstInsertId = classUnderTest.insert(XChangeRate.forEuroDollarAndTimestamp(EURO_RATE, DOLLAR_RATE, now));
        assertThat(firstInsertId, is(not(nullValue())));

        // store second record with EARLIER check timestamp
        Date before = Date.from(now.toInstant().minusMillis(10));
        Long secondInsertId = classUnderTest.insert(XChangeRate.forEuroDollarAndTimestamp(EURO_RATE_2, DOLLAR_RATE_2, before));
        assertThat(secondInsertId, is(not(nullValue())));

        // expect read first record because it's the one with latest check timestamp
        XChangeRate reloadedXchangeRate = classUnderTest.findLatestXChangeRate();
        assertThat(reloadedXchangeRate.id, is(firstInsertId));
        assertThat(reloadedXchangeRate.btcEuroRate, is(EURO_RATE));
        assertThat(reloadedXchangeRate.btcDollarRate, is(DOLLAR_RATE));
    }

    @Test
    public void testFindAll() throws Exception {
        Date now = new Date();
        Date later = Date.from(now.toInstant().plusMillis(10));
        classUnderTest.insert(XChangeRate.forEuroDollarAndTimestamp(EURO_RATE, DOLLAR_RATE, now));
        classUnderTest.insert(XChangeRate.forEuroDollarAndTimestamp(EURO_RATE_2, DOLLAR_RATE_2, later));

        List<XChangeRate> allXchangeRates = classUnderTest.findAll();
        assertThat(allXchangeRates, hasSize(2));
        assertThat(allXchangeRates.get(0).btcEuroRate, is(EURO_RATE));
        assertThat(allXchangeRates.get(1).btcDollarRate, is(DOLLAR_RATE_2));
    }

    @Test
    public void testFindByMinimalCheckedAt() throws Exception {
        Date now = new Date();
        Date before10Hours = Date.from(now.toInstant().minus(10, ChronoUnit.HOURS));
        Date before20Hours = Date.from(now.toInstant().minus(20, ChronoUnit.HOURS));
        classUnderTest.insert(XChangeRate.forEuroDollarAndTimestamp(EURO_RATE, DOLLAR_RATE, before20Hours));
        classUnderTest.insert(XChangeRate.forEuroDollarAndTimestamp(EURO_RATE_2, DOLLAR_RATE_2, before10Hours));
        classUnderTest.insert(XChangeRate.forEuroDollarAndTimestamp(EURO_RATE_3, DOLLAR_RATE_3, now));

        List<XChangeRate> ratesLast10Hours = classUnderTest.findByMinimumCheckedAt(before10Hours);
        assertThat(ratesLast10Hours, hasSize(2));
        assertThat(ratesLast10Hours.get(0).btcEuroRate, is(EURO_RATE_2));
        assertThat(ratesLast10Hours.get(1).btcDollarRate, is(DOLLAR_RATE_3));
    }
}
