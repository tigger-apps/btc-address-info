# Bitcoin Address Info

A simple Android app to scan a Bitcoin address as QR code and print out following information:

* detected address
* total number of all transactions (funding and spending)
* total sum of unspent TXO in BTC/mBTC
* corresponding fiat amount in US$ and €
* display current address as QR code for further use
* show rate statistics based on previous checks
* simple settings page

| Main activity | Address details | Address as QR code |
|:-------------:|:---------------:|:------------------:|
| <img src="address_list.png"> |  <img src="address_details.png"> | <img src="address_qrcode.png"> |

| Settings | Rate statistics | Widget config |
|:-----------:|:----------------:|:----------------:|
| <img src="settings.png"> |  <img src="rate_statistics.png"> | <img src="widget_config.png"> |

Also, this app provides a small widget which displays a small widget which shows the latest exchange rate.

*Note: Widget itself does not ask the network - so use the background service to fetch new rates periodically.*

<img width="30%" src="widget.png"> 

## License

This software is free software and licensed under GNU GPL v3. See [LICENSE.md](LICENSE.md) for
full license text.

## System requirement

Android 8.0 (SDK version 26+)

Tested with:

* Android 8.0
* Android 10.0
* Android 11.0

## Download APK

Of course, you can build the software by your own using Android SDK.

For these who just want a ready-to-use precompiled version, please download latest version: [btc-address-info-0.0.9.apk](app/release/btc-address-info-0.0.9.apk)

## Android permissions

### ACCESS_NETWORK_STATE

Used to detect "no network available" before fetching newest information (xchange rate, address info).

### CAMERA

Camera permission is used to scan QR codes inside this app.

### INTERNET

Read xchange rate and address info via **network services**, see next section.


## Network services

### Address data source

Address is checked against [Blockstream API](https://github.com/Blockstream/esplora/blob/master/API.md):

`GET https://blockstream.info/api/address/YOURADDRESS`

```json
{
  "address":"BTCAddress",
  "chain_stats": {
    "funded_txo_count":1,
	"funded_txo_sum":250000,
	"spent_txo_count":0,
    "spent_txo_sum":0,
	"tx_count":1
  },
  "mempool_stats":{
    "funded_txo_count":0,
	"funded_txo_sum":0,
	"spent_txo_count":0,
	"spent_txo_sum":0,
	"tx_count":0
  }
}
```

Note:

* Num TX contains both num TX in blockchain and num TX in mempool.
* Unspent TXO contains both sum unspent TXO in blockchain and sum unspend TXO in mempool.

### Exchange rate source

Current exchange rate is fetched from [CoinDesk API](https://www.coindesk.com/coindesk-api)

`GET https://api.coindesk.com/v1/bpi/currentprice.json`

```json
{
  "time":{
    "updated":"Mar 25, 2021 18:59:00 UTC",
    "updatedISO":"2021-03-25T18:59:00+00:00",
    "updateduk":"Mar 25, 2021 at 18:59 GMT"
  },
  "disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
  "chartName":"Bitcoin",
  "bpi":{
    "USD":{"code":"USD","symbol":"&#36;","rate":"52,300.4150","description":"United States Dollar","rate_float":52300.415},
    "GBP":{"code":"GBP","symbol":"&pound;","rate":"38,070.1520","description":"British Pound Sterling","rate_float":38070.152},
    "EUR":{"code":"EUR","symbol":"&euro;","rate":"44,418.3764","description":"Euro","rate_float":44418.3764}
    }
  }
```

## Embedded services

QR scanner / QR generator is using [ZXing library](https://github.com/zxing/zxing).

API calls are performed by [Fast Android Networking library](https://github.com/amitshekhariitbhu/Fast-Android-Networking).

JSON handling is supported by [JSON.org library](https://github.com/stleary/JSON-java).

Bitcoin logo created by [AllienWorks](https://github.com/AllienWorks/cryptocoins)

## Run tests

### Unit tests

Run `./gradlew clean test` on console to perform all unit tests.

To run unit tests you don't need any connected device.

### Android tests

**BE CAREFUL: A test run will remove an existing version of this app!**

Choose wisely which device you want to use as host for your tests - running android tests will install the app freshly and
will remove any existing data. So it may be wise to disconnect your real device and start an emulator for this tests.

Run `./gradlew cAT --info` (or `./gradlew connectedAndroidTest --info`).
